#include <iostream>
#include <map>

bool isValid(std::map<std::string, bool>& fields) {
    bool succeed = true;
    for (std::map<std::string, bool>::iterator it = fields.begin(); it != fields.end(); ++it)
    {
        if (!it->second) {
            succeed = false; 
        }
        it->second = false;
    }
    return succeed;
}

int main(int argc, char** argv) {
    std::string line;
    std::map<std::string, bool> fields = {
        {"byr", false},
        {"iyr", false},
        {"eyr", false},
        {"hgt", false},
        {"hcl", false},
        {"ecl", false},
        {"pid", false}
    };
    size_t valids = 0;


    while (std::getline(std::cin, line)) {
        if (line == "") {
            valids += isValid(fields);
        }
        else {
            while (line != "") {
                std::string field = line.substr(0, line.find(':'));
                if (fields.count(field)) {
                    fields[field] = true;
                }
                if (line.find(' ') != std::string::npos) {
                    line = line.substr(line.find(' ') + 1);
                }
                else {
                    line = "";
                }
            }
        }
    }
    valids += isValid(fields);

    std::cout << valids << std::endl;
}