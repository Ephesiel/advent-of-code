#include <iostream>
#include <map>
#include <regex>

bool isValid(std::map<std::string, std::string>& fields) {
    for (auto it = fields.begin(); it != fields.end(); ++it) {
        if (it->first != "cid" && it->second.empty()) {
            return false;
        }

        if (it->first == "byr") {
            if (! std::regex_match(it->second, std::regex("^[0-9]{4}$")) || atoi(it->second.c_str()) < 1920 || atoi(it->second.c_str()) > 2002) {
                return false;
            }
        }
        else if (it->first == "iyr") {
            if (! std::regex_match(it->second, std::regex("^[0-9]{4}$")) || atoi(it->second.c_str()) < 2010 || atoi(it->second.c_str()) > 2020) {
                return false;
            }
        }
        else if (it->first == "eyr") {
            if (! std::regex_match(it->second, std::regex("^[0-9]{4}$")) || atoi(it->second.c_str()) < 2020 || atoi(it->second.c_str()) > 2030) {
                return false;
            }
        }
        else if (it->first == "hgt") {
            if (it->second.substr(it->second.size() - 2) == "cm") {
                if (! std::regex_match(it->second, std::regex("^[0-9]+cm$")) || atoi(it->second.c_str()) < 150 || atoi(it->second.c_str()) > 193) {
                    return false;
                }
            }
            else if (it->second.substr(it->second.size() - 2) == "in") {
                if (! std::regex_match(it->second, std::regex("^[0-9]+in$")) || atoi(it->second.c_str()) < 59 || atoi(it->second.c_str()) > 76) {
                    return false;
                }
            }
            else {
                return false;
            }
        }
        else if (it->first == "hcl") {
            if (! std::regex_match(it->second, std::regex("^#([0-9a-f]){6}$"))) {
                return false;
            }
        }
        else if (it->first == "ecl") {
            if (! std::regex_match(it->second, std::regex("^(amb|blu|brn|gry|grn|hzl|oth)$"))) {
                return false;
            }
        }
        else if (it->first == "pid") {
            if (! std::regex_match(it->second, std::regex("^[0-9]{9}$"))) {
                return false;
            }
        }
    }

    return true;
}

int main(int argc, char** argv) {
    std::string line;
    std::map<std::string, std::string> fields = {
        {"byr", ""},
        {"iyr", ""},
        {"eyr", ""},
        {"hgt", ""},
        {"hcl", ""},
        {"ecl", ""},
        {"pid", ""},
        {"cid", ""}
    };
    size_t valids = 0;
    size_t count = 0;


    while (std::getline(std::cin, line)) {
        if (line == "") {
            count++;
            valids += isValid(fields);
            fields["byr"] = "";
            fields["iyr"] = "";
            fields["eyr"] = "";
            fields["hgt"] = "";
            fields["hcl"] = "";
            fields["ecl"] = "";
            fields["pid"] = "";
            fields["cid"] = "";
        }
        else {
            while (line != "") {
                size_t pos1 = line.find(':');
                size_t pos2 = line.find(' ');

                std::string field = line.substr(0, pos1);
                if (pos2 != std::string::npos) {
                    fields[field] = line.substr(pos1 + 1, pos2 - pos1 - 1);
                    line = line.substr(pos2 + 1);
                }
                else {
                    fields[field] = line.substr(pos1 + 1);
                    line = "";
                }
            }
        }
    }
    valids += isValid(fields);

    std::cout << valids << std::endl;
}