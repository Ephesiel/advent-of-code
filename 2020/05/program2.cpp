#include <iostream>

int main(int argc, char** argv) {
    std::string line;
    bool ids[1024];
    for (size_t i = 0; i < 1024; ++i) {
        ids[i] = false;
    }
    
    while (std::getline(std::cin, line)) {
        size_t id = 0;
        size_t i = 0;

        for (; i < 7; ++i) {
            id |= (line[i] == 'B') << (9 - i);
        }

        for (; i < 10; ++i) {
            id |= (line[i] == 'R') << (9 - i);
        }
        
        ids[id] = true;
    }

    for (size_t i = 1; i < 1023; ++i) {
        if (!ids[i] && ids[i - 1] && ids[i + 1]) {
            std::cout << i << std::endl;
        }
    }
    
    return 0;
}