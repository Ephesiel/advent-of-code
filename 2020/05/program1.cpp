#include <iostream>

int main(int argc, char** argv) {
    std::string line;
    size_t max_id = 0;
    
    while (std::getline(std::cin, line)) {
        size_t id = 0;
        size_t i = 0;

        for (; i < 7; ++i) {
            id |= (line[i] == 'B') << (9 - i);
        }

        for (; i < 10; ++i) {
            id |= (line[i] == 'R') << (9 - i);
        }

        if (id > max_id) {
            max_id = id;
        }
    }

    std::cout << max_id << std::endl;
    
    return 0;
}