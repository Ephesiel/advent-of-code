#include <iostream>
#include <vector>

int main(int argc, char** argv) {
    std::string line;
    size_t index = 0;
    size_t trees = 0;

    while (std::getline(std::cin, line)) {
        trees += line[index] == '#';
        index += 3;
        index %= line.size();
    }

    std::cout << trees << std::endl;
    return 0;
}