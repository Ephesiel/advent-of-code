#include <iostream>
#include <vector>

int main(int argc, char** argv) {
    std::string line;
    size_t index1 = 0;
    size_t index2 = 0;
    size_t index3 = 0;
    size_t index4 = 0;
    size_t index5 = 0;
    size_t trees1 = 0;
    size_t trees2 = 0;
    size_t trees3 = 0;
    size_t trees4 = 0;
    size_t trees5 = 0;
    size_t count = 0;

    while (std::getline(std::cin, line)) {
        if (!(count++ & 1)) {
            trees5 += line[index5] == '#';
            index5 += 1;
            index5 %= line.size();
        }
        trees1 += line[index1] == '#';
        trees2 += line[index2] == '#';
        trees3 += line[index3] == '#';
        trees4 += line[index4] == '#';

        index1 += 1;
        index1 %= line.size();
        index2 += 3;
        index2 %= line.size();
        index3 += 5;
        index3 %= line.size();
        index4 += 7;
        index4 %= line.size();
    }

    std::cout << trees1 * trees2 * trees3 * trees4 * trees5 << std::endl;

    return 0;
}