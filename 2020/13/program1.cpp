#include <iostream>
#include <vector>

int main(int argc, char** argv) {
    std::string line;
    std::getline(std::cin, line);
    size_t arriving = atoi(line.c_str());
    std::getline(std::cin, line);
    
    std::vector<size_t> buses;

    size_t pos;
    while ((pos = line.find(",")) != std::string::npos) {
        std::string bus = line.substr(0, pos);
        if (bus != "x") {
            buses.push_back(atoi(bus.c_str()));
        }
        line = line.substr(pos + 1);
    }
    if (line != "x") {
        buses.push_back(atoi(line.c_str()));
    }

    size_t bestBus = buses[0];
    for (size_t bus : buses) {
        std::cout << bus - arriving % bus << std::endl;
        if (bestBus - arriving % bestBus > bus - arriving % bus) {
            bestBus = bus;
        }
    }

    std::cout << bestBus * (bestBus - arriving % bestBus) << std::endl;

    return 0;
}