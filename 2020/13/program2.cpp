#include <iostream>
#include <vector>

struct Bus {
    size_t id;
    size_t offset;
};

int main(int argc, char** argv) {
    std::string line;
    std::getline(std::cin, line);
    std::getline(std::cin, line);
    
    std::vector<Bus> buses;

    size_t pos;
    size_t offset = 0;
    while ((pos = line.find(",")) != std::string::npos) {
        std::string ss = line.substr(0, pos);
        if (ss != "x") {
            Bus bus;
            bus.offset = offset;
            bus.id = atoi(ss.c_str());
            buses.push_back(bus);
        }
        line = line.substr(pos + 1);
        offset++;
    }
    if (line != "x") {
        Bus bus;
        bus.offset = offset;
        bus.id = atoi(line.c_str());
        buses.push_back(bus);
    }

    size_t timestamp = 0;
    size_t increaseBy = buses[0].id; 
    for (size_t i = 1; i < buses.size(); ++i) {
        bool foundfirst = false;
        // On met ça parce que le début, tout commence à 0, pour avoir un vrai offset
        // il faut avoir pu trouver deux fois un match.
        // Sauf pour le dernier, où le résultat est le premier match trouvé
        bool foundsecond = false;
        size_t firsttimestamp = 0;
        while (!foundsecond) {
            if ((timestamp + buses[i].offset) % buses[i].id == 0) {
                if (!foundfirst) {
                    firsttimestamp = timestamp;
                    foundfirst = true;

                    if (i == buses.size() - 1) {
                        foundsecond = true;
                    }
                }
                else {
                    foundsecond = true;
                }
            }

            if (!foundsecond) {
                timestamp += increaseBy;
            }
        }
        increaseBy = timestamp - firsttimestamp;
    }

    std::cout << timestamp << std::endl;

    return 0;
}