#include <iostream>
#include <vector>

int main(int argc, char** argv) {
    std::string line;
    std::vector<size_t> numbers;

    while (std::getline(std::cin, line)) {
        numbers.push_back(atoi(line.c_str()));
    }

    size_t i = 24;
    bool founded = true;
    while (founded && ++i < numbers.size()) {
        founded = false;
        size_t j = i - 25;
        while (!founded && j < i - 1) {
            size_t k = j + 1;
            while (!founded && k < i) {
                founded = numbers[j] + numbers[k] == numbers[i];
                ++k;
            }
            ++j;
        }
    }

    size_t sum = 0;
    size_t beg = 0;
    size_t end = 0;
    while (sum != numbers[i] && end < numbers.size()) {
        sum += numbers[end++];
        while (sum > numbers[i]) {
            sum -= numbers[beg++];
        }
    }

    size_t min = numbers[beg];
    size_t max = numbers[end];
    for (size_t j = beg; j < end; ++j) {
        if (numbers[j] < min) {
            min = numbers[j];
        }
        if (numbers[j] > max) {
            max = numbers[j];
        }
    }  

    std::cout << max + min << std::endl;

    return 0;
}