#include <iostream>
#include <vector>

int main(int argc, char** argv) {
    std::string line;
    std::vector<size_t> numbers;

    while (std::getline(std::cin, line)) {
        numbers.push_back(atoi(line.c_str()));
    }

    size_t i = 24;
    bool founded = true;
    while (founded && ++i < numbers.size()) {
        founded = false;
        size_t j = i - 25;
        while (!founded && j < i - 1) {
            size_t k = j + 1;
            while (!founded && k < i) {
                founded = numbers[j] + numbers[k] == numbers[i];
                ++k;
            }
            ++j;
        }
    }

    std::cout << numbers[i] << std::endl;

    return 0;
}