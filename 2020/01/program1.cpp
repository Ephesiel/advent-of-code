#include <iostream>
#include <vector>

int main(int argc, char** argv) {
    std::string ligne;
    std::vector<int> valeurs;

    int output = 0;

    while (std::getline(std::cin, ligne) && output == 0) {
        int valeur = atoi(ligne.c_str());
        size_t i = 0;

        while (output == 0 && i < valeurs.size()) {
            if (valeurs[i] + valeur == 2020) {
                output = valeurs[i] * valeur;
            }
            ++i;
        }
        valeurs.push_back(valeur);
    }

    std::cout << output << std::endl;

    return 0;
}