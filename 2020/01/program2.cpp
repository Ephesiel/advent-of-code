#include <iostream>
#include <vector>

int main(int argc, char** argv) {
    std::string ligne;
    std::vector<int> valeurs;

    int output = 0;

    while (std::getline(std::cin, ligne) && output == 0) {
        int valeur = atoi(ligne.c_str());
        size_t i = 0;

        while (output == 0 && i < valeurs.size()) {
            size_t j = i + 1;
            while (output == 0 && j < valeurs.size()) {
                if (valeurs[i] + valeurs[j] + valeur == 2020) {
                    output = valeurs[i] * valeurs[j] * valeur;
                }
                ++j;
            }
            ++i;
        }
        valeurs.push_back(valeur);
    }

    std::cout << output << std::endl;

    return 0;
}