#!/usr/bin/env python3

# input
values = sorted(list(map(int, open("input.txt").readlines())))
n = len(values)

for i in range(n):
    for j in range(i + 1, n):
        if values[i] + values[j] == 2020:
            print(values[i] * values[j])

for i in range(n):
    for j in range(i + 1, n):
        for k in range(j + 1, n):
            if values[i] + values[j] + values[k] == 2020:
                print(values[i] * values[j] * values[k])