#include <iostream>
#include <algorithm>
#include <list>

size_t calcul(std::string& line) {
    size_t n = 0;
    std::list<size_t> mul;
    while (!line.empty() && line[0] != ')') {
        if (line[0] == '*') {
            line = line.substr(1);
            mul.push_back(n);
            n = 0;
            continue;
        }
        else if (line[0] == '+') {
            line = line.substr(1);
        }

        size_t val = 0;

        if (line[0] == '(') {
            line = line.substr(1);
            val = calcul(line);
            line = line.substr(1);
        }
        else {
            size_t min = std::min(std::min(line.size(), line.find('*')), std::min(line.find('+'), line.find(')')));
            val = atoi(line.substr(0, min).c_str());
            line = line.substr(min);
        }

        n += val;
    }

    for (size_t val : mul) {
        n *= val;
    }
    return n;
}

int main(int argc, char** argv) {
    size_t n = 0;

    std::string line;
    while (std::getline(std::cin, line)) {
        line.erase(std::remove(line.begin(), line.end(), ' '), line.end());
        n += calcul(line);
    }

    std::cout << n << std::endl;

    return 0;
}