#include <iostream>
#include <algorithm>

size_t calcul(std::string& line) {
    size_t n = 0;
    while (!line.empty() && line[0] != ')') {
        bool addition = true;
        if (line[0] == '*') {
            line = line.substr(1);
            addition = false;
        }
        else if (line[0] == '+') {
            line = line.substr(1);
        }

        size_t val = 0;

        if (line[0] == '(') {
            line = line.substr(1);
            val = calcul(line);
            line = line.substr(1);
        }
        else {
            size_t min = std::min(std::min(line.size(), line.find('*')), std::min(line.find('+'), line.find(')')));
            val = atoi(line.substr(0, min).c_str());
            line = line.substr(min);
        }

        if (addition) {
            n += val;
        }
        else {
            n *= val;
        }
    }
    
    return n;
}

int main(int argc, char** argv) {
    size_t n = 0;

    std::string line;
    while (std::getline(std::cin, line)) {
        line.erase(std::remove(line.begin(), line.end(), ' '), line.end());
        n += calcul(line);
    }

    std::cout << n << std::endl;

    return 0;
}