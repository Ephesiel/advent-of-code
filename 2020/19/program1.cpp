#include <iostream>
#include <list>
#include <vector>

struct Rule {
    char match = 0;
    std::list<std::list<size_t>> rules;
};

bool verifyRule(const std::vector<Rule>& rules, const std::string& line, size_t ruleNumber, size_t& charNumber) {
    if (rules[ruleNumber].match != 0) {
        return charNumber < line.size() && line[charNumber++] == rules[ruleNumber].match;
    }
    else {
        size_t cp = charNumber;

        for (const std::list<size_t> rule : rules[ruleNumber].rules) {
            bool succeed = true;
            for (size_t n : rule) {
                if (!(succeed = verifyRule(rules, line, n, charNumber))) {
                    break;
                }
            }
            if (succeed) {
                return true;
            }
            charNumber = cp;
        }

        return false;
    }
}

int main(int argc, char** argv) {
    std::vector<Rule> rules;
    std::string line;
    while (std::getline(std::cin, line) && !line.empty()) {
        size_t pos = line.find(": ");
        size_t num = std::stoi(line.substr(0, pos));
        if (num >= rules.size()) {
            rules.resize(num + 1);
        }
        line = line.substr(pos + 2);

        pos = line.find("\"");
        if (pos != std::string::npos) {
            rules[num].match = line[pos + 1];
        }
        else {
            std::list<size_t> numbers;
            while (!line.empty()) {
                size_t space = line.find(" ");
                size_t pipe = line.find(" | ");
                if (space == std::string::npos && pipe == std::string::npos) {
                    numbers.push_back(std::stoi(line));
                    rules[num].rules.push_back(numbers);
                    line = "";
                }
                else if (pipe <= space) {
                    numbers.push_back(std::stoi(line.substr(0, pipe)));
                    rules[num].rules.push_back(numbers);
                    numbers.clear();
                    line = line.substr(pipe + 3);
                }
                else { 
                    numbers.push_back(std::stoi(line.substr(0, space)));
                    line = line.substr(space + 1);
                }
            }
        }
    }

    size_t count = 0;

    while (std::getline(std::cin, line) && !line.empty()) {
        size_t charNumber = 0;
        verifyRule(rules, line, 0, charNumber);
        count += charNumber == line.size();
    }

    std::cout << count << std::endl;

    return 0;
}