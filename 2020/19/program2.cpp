#include <iostream>
#include <list>
#include <vector>
#include <regex>

struct Rule {
    std::string match = "";
    std::list<std::list<size_t>> rules;
};

std::string regex(const std::vector<Rule>& rules, size_t ruleNumber) {
    std::string r = "";
    if (!rules[ruleNumber].match .empty()) {
        r = rules[ruleNumber].match;
    }
    else if (ruleNumber == 8) {
        r = regex(rules, 42) + '+';
    }
    else if (ruleNumber == 11) {
        std::string r42 = regex(rules, 42);
        std::string r31 = regex(rules, 31);
        r += '(';
        for (size_t i = 0; i < 5; ++i) {
            std::string n = "";
            for (size_t j = i; j < 5; ++j) {
                n = r42 + n + r31;
            }
            r += n + '|';
        }
        r[r.size() - 1] = ')';
    }
    else {
        r += '(';
        for (const std::list<size_t> rule : rules[ruleNumber].rules) {
            for (size_t n : rule) {
                r += regex(rules, n);
            }
            r += '|';
        }
        r[r.size() - 1] = ')';
    }
    return r;
}

int main(int argc, char** argv) {
    std::vector<Rule> rules;
    std::string line;
    while (std::getline(std::cin, line) && !line.empty()) {
        if (line == "8: 42") {
            line = "8: 42 | 42 8";
        }
        else if (line == "11: 42 31") {
            line = "11: 42 31 | 42 11 31";
        }
        size_t pos = line.find(": ");
        size_t num = std::stoi(line.substr(0, pos));
        if (num >= rules.size()) {
            rules.resize(num + 1);
        }
        line = line.substr(pos + 2);

        pos = line.find("\"");
        if (pos != std::string::npos) {
            rules[num].match = line[pos + 1];
        }
        else {
            std::list<size_t> numbers;
            while (!line.empty()) {
                size_t space = line.find(" ");
                size_t pipe = line.find(" | ");
                if (space == std::string::npos && pipe == std::string::npos) {
                    numbers.push_back(std::stoi(line));
                    rules[num].rules.push_back(numbers);
                    line = "";
                }
                else if (pipe <= space) {
                    numbers.push_back(std::stoi(line.substr(0, pipe)));
                    rules[num].rules.push_back(numbers);
                    numbers.clear();
                    line = line.substr(pipe + 3);
                }
                else { 
                    numbers.push_back(std::stoi(line.substr(0, space)));
                    line = line.substr(space + 1);
                }
            }
        }
    }

    size_t count = 0;
    std::string r = '^' + regex(rules, 0) + '$';

    while (std::getline(std::cin, line) && !line.empty()) {
        count += std::regex_match(line, std::regex(r));
    }

    std::cout << count << std::endl;

    return 0;
}