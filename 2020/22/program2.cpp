#include <iostream>
#include <queue>
#include <list>
#include <algorithm>

typedef size_t Card;

size_t game(std::queue<Card>& deck1, std::queue<Card>& deck2) {
    std::list<std::pair<std::queue<Card>, std::queue<Card>>> cache;
    while (!deck1.empty() && !deck2.empty()) {
        std::pair<std::queue<Card>, std::queue<Card>> actual = std::make_pair(deck1, deck2);
        if (std::find(cache.begin(), cache.end(), actual) != cache.end()) {
            return 1;
        }
        cache.push_back(actual);

        Card card1 = deck1.front();
        Card card2 = deck2.front();
        deck1.pop();
        deck2.pop();

        size_t winner = card1 > card2 ? 1 : 2;
        if (card1 <= deck1.size() && card2 <= deck2.size()) {
            std::queue<Card> copy1 = deck1;
            std::queue<Card> copy2 = deck2;
            std::queue<Card> recurs1;
            std::queue<Card> recurs2;
            for (size_t i = 0; i < card1; ++i) {
                recurs1.push(copy1.front());
                copy1.pop();
            }
            for (size_t i = 0; i < card2; ++i) {
                recurs2.push(copy2.front());
                copy2.pop();
            }
            winner = game(recurs1, recurs2);
        }

        if (winner == 1) {
            deck1.push(card1);
            deck1.push(card2);
        }
        else {
            deck2.push(card2);
            deck2.push(card1);
        }
    }

    return deck1.empty() + 1;
} 

int main(int argc, char** argv) {
    std::queue<Card> deck1;
    std::queue<Card> deck2;

    std::string line;
    bool first = true;
    std::getline(std::cin, line);
    while (std::getline(std::cin, line)) {
        if (first) {
            if (line.empty()) {
                first = false;
                std::getline(std::cin, line);
            }
            else {
                deck1.push(std::stoi(line));
            }
        }
        else {
            deck2.push(std::stoi(line));
        }
    }
    
    std::queue<Card>& winner = game(deck1, deck2) == 1 ? deck1 : deck2;
    size_t multiplier = winner.size();
    size_t result = 0;

    while (!winner.empty()) {
        Card topCard = winner.front();
        winner.pop();
        result += topCard * multiplier--;
    }

    std::cout << result << std::endl;

    return 0;
}