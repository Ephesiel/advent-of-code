#include <iostream>
#include <queue>

typedef size_t Card;

int main(int argc, char** argv) {
    std::queue<Card> deck1;
    std::queue<Card> deck2;

    std::string line;
    bool first = true;
    std::getline(std::cin, line);
    while (std::getline(std::cin, line)) {
        if (first) {
            if (line.empty()) {
                first = false;
                std::getline(std::cin, line);
            }
            else {
                deck1.push(std::stoi(line));
            }
        }
        else {
            deck2.push(std::stoi(line));
        }
    }
    
    while (!deck1.empty() && !deck2.empty()) {
        Card card1 = deck1.front();
        Card card2 = deck2.front();
        deck1.pop();
        deck2.pop();
        if (card1 > card2) {
            deck1.push(card1);
            deck1.push(card2);
        }
        else {
            deck2.push(card2);
            deck2.push(card1);
        }
    }

    std::queue<Card>& winner = deck1.empty() ? deck2 : deck1;
    size_t multiplier = winner.size();
    size_t result = 0;

    while (!winner.empty()) {
        Card topCard = winner.front();
        winner.pop();
        result += topCard * multiplier--;
    }

    std::cout << result << std::endl;

    return 0;
}