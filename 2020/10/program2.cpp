#include <iostream>
#include <set>
#include <vector>

int main(int argc, char** argv) {
    std::string line;
    std::set<size_t> joltages;
    joltages.emplace(0);

    while (std::getline(std::cin, line)) {
        joltages.emplace(atoi(line.c_str()));
    }
    
    std::vector<size_t> buffer(joltages.size(), 0);
    size_t last = joltages.size() - 1;
    buffer[last] = 1;

    for (std::set<size_t>::iterator it = joltages.end(); it != joltages.begin(); last--) {
        std::set<size_t>::iterator cit = --it;
        size_t indice = 0;

        while (++cit != joltages.end() && *cit - *it <= 3) {
            buffer[last] += buffer[last + ++indice];
        }
    }

    std::cout << buffer[0] << std::endl;

    return 0;
}