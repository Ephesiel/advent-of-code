#include <iostream>
#include <set>

int main(int argc, char** argv) {
    std::string line;
    std::set<size_t> joltages;

    while (std::getline(std::cin, line)) {
        joltages.emplace(atoi(line.c_str()));
    }
    
    size_t diffs[3];
    diffs[0] = 0;
    diffs[1] = 0;
    diffs[2] = 1;
    size_t jolt = 0;
    for (size_t joltage : joltages) {
        diffs[joltage - jolt - 1]++;
        jolt = joltage;
    }

    std::cout << diffs[0] * diffs[2] << std::endl;

    return 0;
}