#!/usr/bin/env python3

# input
values = [0] + sorted(list(map(int, open("input.txt").readlines())))

# part 1
one = 0
three = 1
for i in range(len(values) - 1):
    diff = values[i + 1] - values[i]
    if diff == 1:
        one += 1
    elif diff == 3:
        three += 1

print(one * three)

# part 2
N = len(values)
T = [None] * N

def arrangements(s):
    if s == N - 1:
        T[s] = 1
        return 1
    if T[s] is not None:
        return T[s]
    
    arr = 0
    if s < N - 1 and values[s + 1] - values[s] <= 3:
        arr += arrangements(s + 1)
    if s < N - 2 and values[s + 2] - values[s] <= 3:
        arr += arrangements(s + 2)
    if s < N - 3 and values[s + 3] - values[s] <= 3:
        arr += arrangements(s + 3)
    
    T[s] = arr
    return arr

print(arrangements(0))