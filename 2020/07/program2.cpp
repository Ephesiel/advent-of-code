#include <iostream>
#include <map>
#include <vector>
#include <list>
#include <set>

struct Bag {
    std::vector<Bag*> child;
    std::vector<size_t> number;
};

std::vector<std::string> split(const std::string& s, const std::string& del) {
    std::vector<std::string> result;
    size_t start = 0;
    size_t end = s.find(del);

    while (end != std::string::npos) {
        result.push_back(s.substr(start, end - start));
        start = end + del.size();
        end = s.find(del, start);
    }

    result.push_back(s.substr(start));
    return result;
}

size_t number_of_bags(Bag* bag) {
    size_t result = 1;
    for (size_t i = 0; i < bag->child.size(); ++i) {
        result += bag->number[i] * number_of_bags(bag->child[i]);
    }
    return result;
}

int main(int argc, char** argv) {
    std::string line;
    std::map<std::string, Bag*> bags;

    while (std::getline(std::cin, line)) {
        std::vector<std::string> els = split(line, " bags contain ");
        std::string name = els[0];
        
        if (bags.find(name) == bags.end()) {
            bags.emplace(name, new Bag);
        }

        for (std::string s : split(els[1], ", ")) {
            std::vector<std::string> words = split(s, " ");
            if (words.size() == 4) {
                std::string n = words[1] + ' ' + words[2];
        
                if (bags.find(n) == bags.end()) {
                    bags.emplace(n, new Bag);
                }

                bags[name]->child.push_back(bags[n]);
                bags[name]->number.push_back(atoi(words[0].c_str()));
            }
        }
    }

    std::cout << number_of_bags(bags["shiny gold"]) - 1 << std::endl;

    return 0;
}