#include <iostream>
#include <map>
#include <vector>
#include <list>
#include <set>

struct Bag {
    std::list<Bag*> containers;
    std::string name;
};

std::vector<std::string> split(const std::string& s, const std::string& del) {
    std::vector<std::string> result;
    size_t start = 0;
    size_t end = s.find(del);

    while (end != std::string::npos) {
        result.push_back(s.substr(start, end - start));
        start = end + del.size();
        end = s.find(del, start);
    }

    result.push_back(s.substr(start));
    return result;
}

std::set<std::string> find_child(Bag* bag) {
    std::set<std::string> result;
    for (Bag* b : bag->containers) {
        result.emplace(b->name);
        std::set<std::string> bResult = find_child(b);
        result.insert(bResult.begin(), bResult.end());
    }
    return result;
}

int main(int argc, char** argv) {
    std::string line;
    std::map<std::string, Bag*> bags;

    while (std::getline(std::cin, line)) {
        std::vector<std::string> els = split(line, " bags contain ");
        std::string name = els[0];
        
        if (bags.find(name) == bags.end()) {
            Bag* bag = new Bag;
            bag->name = name;
            bags.emplace(name, bag);
        }

        Bag* container = bags[name];

        for (std::string s : split(els[1], ", ")) {
            std::vector<std::string> words = split(s, " ");
            if (words.size() == 4) {
                std::string n = words[1] + ' ' + words[2];
        
                if (bags.find(n) == bags.end()) {
                    Bag* bag = new Bag;
                    bag->name = n;
                    bags.emplace(n, bag);
                }

                bags[n]->containers.push_back(container);
            }
        }
    }

    std::cout << find_child(bags["shiny gold"]).size() << std::endl;

    return 0;
}