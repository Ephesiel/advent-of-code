#include <iostream>

void cutString(const std::string& s, const char d, std::string& l, std::string& r) {
    size_t pos = s.find(d);
    r = "";
    l = s;
    if (pos != std::string::npos) {
        l = s.substr(0, pos);
        r = s.substr(pos + 1);
    }
}

void removeSpaces(std::string& s) {
    size_t first = s.find_first_not_of(' ');
    size_t last = s.find_last_not_of(' ');

    if (first != std::string::npos) {
        if (last != std::string::npos) {
            s = s.substr(first, last + 1);
        }
        else {
            s.substr(first);
        }
    }
    else {
        s = "";
    }
}

int main(int argc, char** argv) {
    int n = 0;
    std::string line;
    while (std::getline(std::cin, line)) {
        std::string l1, l2;
        std::string password;
        std::string letter;
        std::string min, max;

        cutString(line, ':', l1, password);
        removeSpaces(l1);
        removeSpaces(password);
        cutString(l1, ' ', l2, letter);
        cutString(l2, '-', min, max);

        int count = 0;
        char ch = letter[0];
        for (size_t i = 0; i < password.size(); i++) {
            count += password[i] == ch;
        }
        n += (count >= atoi(min.c_str()) && count <= atoi(max.c_str()));
    }

    std::cout << n << std::endl;

    return 0;
}