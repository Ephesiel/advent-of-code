#include <iostream>
#include <vector>

size_t nombreReponses(std::vector<bool>& reponses) {
    size_t nb = 0;
    for (size_t i = 0; i < 26; ++i) {
        nb += reponses[i];
        reponses[i] = false;
    }
    return nb;
}

int main(int argc, char** argv) {
    std::vector<bool> reponses(26, false);
    size_t somme = 0;
    std::string line;

    while(std::getline(std::cin, line)) {
        if (line == "") {
            somme += nombreReponses(reponses);
        }
        else {
            for (size_t i = 0; i < line.size(); ++i) {
                reponses[line[i] - 'a'] = true;
            }
        }
    }
    somme += nombreReponses(reponses);

    std::cout << somme << std::endl;

    return 0;
}