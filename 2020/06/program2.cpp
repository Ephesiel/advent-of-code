#include <iostream>
#include <vector>

size_t nombreReponses(std::vector<bool>& reponses) {
    size_t nb = 0;
    for (size_t i = 0; i < 26; ++i) {
        nb += reponses[i];
        reponses[i] = true;
    }
    return nb;
}

int main(int argc, char** argv) {
    std::vector<bool> reponses(26, true);
    size_t somme = 0;
    std::string line;

    while(std::getline(std::cin, line)) {
        if (line == "") {
            somme += nombreReponses(reponses);
        }
        else {
            for (size_t i = 0; i < 26; ++i) {
                if (reponses[i]) {
                    reponses[i] = line.find((char) 'a' + i) != std::string::npos;
                }
            }
        }
    }
    somme += nombreReponses(reponses);

    std::cout << somme << std::endl;

    return 0;
}