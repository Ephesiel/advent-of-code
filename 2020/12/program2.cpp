#include <iostream>
#include <algorithm>

struct Point {
    int north;
    int east;
};

int main(int argc, char** argv) {
    std::string line;

    Point wp;
    wp.north = 1;
    wp.east = 10;

    Point ship;
    ship.north = 0;
    ship.east = 0;

    while (std::getline(std::cin, line)) {
        char type = line[0];
        int number = atoi(line.substr(1).c_str());

        if (type == 'L') {
            Point cp = wp;
            switch ((number / 90) % 4)
            {
                case 0:
                    break;
                case 1:
                    wp.north = cp.east;
                    wp.east = -cp.north;
                    break;
                case 2:
                    wp.north *= -1;
                    wp.east *= -1;
                    break;
                case 3:
                    wp.north = -cp.east;
                    wp.east = cp.north;
                    break;
            }
        }
        else if (type == 'R') {
            Point cp = wp;
            switch ((number / 90) % 4)
            {
                case 0:
                    break;
                case 1:
                    wp.north = -cp.east;
                    wp.east = cp.north;
                    break;
                case 2:
                    wp.north *= -1;
                    wp.east *= -1;
                    break;
                case 3:
                    wp.north = cp.east;
                    wp.east = -cp.north;
                    break;
            }
        }
        else if (type == 'N') {
            wp.north += number;
        }
        else if (type == 'E') {
            wp.east += number;
        }
        else if (type == 'S') {
            wp.north -= number;
        }
        else if (type == 'W') {
            wp.east -= number;
        }
        else if (type == 'F') {
            ship.east += number * wp.east;
            ship.north += number * wp.north;
        }
        else {
            std::cerr << "WTF " << type << std::endl;
        }
    }

    std::cout << std::abs(ship.north) + std::abs(ship.east) << std::endl;

    return 0;
}