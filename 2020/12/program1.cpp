#include <iostream>
#include <algorithm>

int main(int argc, char** argv) {
    std::string line;
    
    char poles[4] = {'N', 'E', 'S', 'W'};
    char* actualFacing = &poles[1];

    int northPosition = 0;
    int eastPosition = 0;

    while (std::getline(std::cin, line)) {
        char type = line[0];
        int number = atoi(line.substr(1).c_str());
        if (type == 'F') {
            type = *actualFacing;
        }

        switch (type) {
        case 'L':
            for (int i = 0; i < (number / 90) % 4; ++i) {
                if (actualFacing == &poles[0])
                    actualFacing = &poles[3];
                else
                    actualFacing--;
            }
            break;
        case 'R':
            for (int i = 0; i < (number / 90) % 4; ++i) {
                if (actualFacing == &poles[3])
                    actualFacing = &poles[0];
                else
                    actualFacing++;
            }
            break;
        case 'N':
            northPosition += number;
            break;
        case 'E':
            eastPosition += number;
            break;
        case 'S':
            northPosition -= number;
            break;
        case 'W':
            eastPosition -= number;
            break;
        default:
            std::cerr << "WTF " << type << std::endl;
            break;
        }
    }

    std::cout << std::abs(northPosition) + std::abs(eastPosition) << std::endl;

    return 0;
}