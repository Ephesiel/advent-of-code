#include <iostream>
#include <set>

struct Coord {
    int x;
    int y;

    bool operator<(const Coord& o) const {
        return x < o.x || (x == o.x && y < o.y);
    }
};


int main(int argc, char** argv) {
    std::set<Coord> blackTiles;

    std::string line;
    while (std::getline(std::cin, line)) {
        int x = 0;
        int y = 0;
        for (size_t i = 0; i < line.size(); ++i) {
            if (line[i] == 'e') {
                x += 2;
            }
            else if (line[i] == 'w') {
                x -= 2;
            }
            else {
                y += (line[i++] == 'n') ? 1 : -1;
                x += (line[i] == 'e') ? 1 : -1;
            }
        }

        Coord coord;
        coord.x = x;
        coord.y = y;

        if (blackTiles.count(coord)) {
            blackTiles.erase(blackTiles.find(coord));
        }
        else {
            blackTiles.emplace(coord);
        }
    }

    std::cout << blackTiles.size() << std::endl;

    return 0;
}