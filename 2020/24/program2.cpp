#include <iostream>
#include <set>

struct Coord {
    int x;
    int y;

    bool operator<(const Coord& o) const {
        return x < o.x || (x == o.x && y < o.y);
    }
};

size_t blackNeighbours(const std::set<Coord>& blackTiles, const Coord& coord) {
    size_t count = 0;
    Coord c;
    for (c.x = coord.x - 1; c.x <= coord.x + 1; c.x += 2) {
        for (c.y = coord.y - 1; c.y <= coord.y + 1; c.y += 2) {
            count += blackTiles.count(c);
        }
    }

    c.y = coord.y;
    c.x = coord.x - 2;
    count += blackTiles.count(c);
    c.x = coord.x + 2;
    count += blackTiles.count(c);

    return count;
}

void nextDay(std::set<Coord>& blackTiles) {
    const std::set<Coord> cp = blackTiles;
    int minX = 0;
    int minY = 0;
    int maxX = 0;
    int maxY = 0;
    for (const Coord& coord : blackTiles) {
        if (coord.x < minX) minX = coord.x;
        if (coord.y < minY) minY = coord.y;
        if (coord.x > maxX) maxX = coord.x;
        if (coord.y > maxY) maxY = coord.y;
    }
    minX -= 2;
    maxX += 2;
    minY -= 1;
    maxY += 1;

    for (int x = minX; x <= maxX; ++x) {
        for (int y = minY; y <= maxY; ++y) {
            Coord coord;
            coord.x = x;
            coord.y = y;
            size_t neighbours = blackNeighbours(cp, coord);
            if (cp.count(coord)) {
                if (neighbours == 0 || neighbours > 2) {
                    blackTiles.erase(blackTiles.find(coord));
                }
            }
            else if (neighbours == 2) {
                blackTiles.emplace(coord);
            }
        }
    }
}

int main(int argc, char** argv) {
    std::set<Coord> blackTiles;

    std::string line;
    while (std::getline(std::cin, line)) {
        int x = 0;
        int y = 0;
        for (size_t i = 0; i < line.size(); ++i) {
            if (line[i] == 'e') {
                x += 2;
            }
            else if (line[i] == 'w') {
                x -= 2;
            }
            else {
                y += (line[i++] == 'n') ? 1 : -1;
                x += (line[i] == 'e') ? 1 : -1;
            }
        }

        Coord coord;
        coord.x = x;
        coord.y = y;

        if (blackTiles.count(coord)) {
            blackTiles.erase(blackTiles.find(coord));
        }
        else {
            blackTiles.emplace(coord);
        }
    }

    for (size_t i = 0; i < 100; ++i) {
        nextDay(blackTiles);
    }

    std::cout << blackTiles.size() << std::endl;

    return 0;
}