#include <iostream>

size_t findLoopSize(size_t subjectNumber, size_t publicKey) {
    size_t publicKeyToFind = 1;
    size_t loopSize = 0;

    while (publicKeyToFind != publicKey) {
        publicKeyToFind *= subjectNumber;
        publicKeyToFind %= 20201227;
        loopSize++;
    }

    return loopSize;
}

size_t getEncryptionKey(size_t subjectNumber, size_t loopSize) {
    size_t encryptionKey = 1;
    for (size_t i = 0; i < loopSize; ++i) {
        encryptionKey *= subjectNumber;
        encryptionKey %= 20201227;
    }

    return encryptionKey;
}

int main(int argc, char** argv) {
    std::string line;
    std::getline(std::cin, line);
    size_t cardPublicKey = std::stoi(line);
    std::getline(std::cin, line);
    size_t doorPublicKey = std::stoi(line);

    std::cout << getEncryptionKey(doorPublicKey, findLoopSize(7, cardPublicKey)) << std::endl;


    return 0;
}