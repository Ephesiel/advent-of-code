#include <iostream>

void display(bool cells[20][20][20][20]) {
    for (size_t w = 0; w < 20; ++w) {
        for (size_t z = 0; z < 20; ++z) {
            std::cout << "w = " << w << " z = " << z << std::endl;
            for (size_t y = 0; y < 20; ++y) {
                for (size_t x = 0; x < 20; ++x) {
                    std::cout << (cells[w][z][y][x] ? '#' : '.');
                }
                std::cout << std::endl;
            }
            std::cout << std::endl;
        }
    }
}

size_t neighbors(bool cells[20][20][20][20], size_t w, size_t z, size_t y, size_t x) {
    size_t n = 0;
    for (size_t ww = (w == 0 ? 0 : w - 1); ww <= (w == 19 ? 19 : w + 1); ++ww) {
        for (size_t zz = (z == 0 ? 0 : z - 1); zz <= (z == 19 ? 19 : z + 1); ++zz) {
            for (size_t yy = (y == 0 ? 0 : y - 1); yy <= (y == 19 ? 19 : y + 1); ++yy) {
                for (size_t xx = (x == 0 ? 0 : x - 1); xx <= (x == 19 ? 19 : x + 1); ++xx) {
                    if (ww != w || xx != x || yy != y || zz != z) {
                        n += cells[ww][zz][yy][xx];
                    }
                }
            }
        }
    }
    return n;
}

int main(int argc, char** argv) {
    // z, y, x
    bool cells[20][20][20][20];

    for (size_t w = 0; w < 20; ++w) {
        for (size_t z = 0; z < 20; ++z) {
            for (size_t y = 0; y < 20; ++y) {
                for (size_t x = 0; x < 20; ++x) {
                    cells[w][z][y][x] = false;
                }
            }
        }
    }

    std::string line;
    size_t y = 6;
    while (std::getline(std::cin, line) && !line.empty()) {
        for (size_t i = 0; i < line.size(); ++i) {
            if (line[i] == '#') {
                cells[10][10][y][i + 6] = true;
            }
        }
        y++;
    }
    
    for (size_t i = 0; i < 6; ++i) {
        bool cp[20][20][20][20];
        for (size_t w = 0; w < 20; ++w) {
            for (size_t z = 0; z < 20; ++z) {
                for (size_t y = 0; y < 20; ++y) {
                    for (size_t x = 0; x < 20; ++x) {
                        cp[w][z][y][x] = cells[w][z][y][x];
                    }
                }
            }
        }
        for (size_t w = 0; w < 20; ++w) {
            for (size_t z = 0; z < 20; ++z) {
                for (size_t y = 0; y < 20; ++y) {
                    for (size_t x = 0; x < 20; ++x) {
                        size_t n = neighbors(cp, w, z, y, x);
                        cells[w][z][y][x] = (n == 3 || (n == 2 && cp[w][z][y][x]));
                    }
                }
            }
        }
    }

    size_t n = 0;
    for (size_t w = 0; w < 20; ++w) {
        for (size_t z = 0; z < 20; ++z) {
            for (size_t y = 0; y < 20; ++y) {
                for (size_t x = 0; x < 20; ++x) {
                    n += cells[w][z][y][x];
                }
            }
        }
    }

    std::cout << n << std::endl;
    return 0;
}