#include <iostream>
#include <map>

typedef unsigned long long Adress;

int main(int argc, char** argv) {
    unsigned long long mask[36];
    std::map<Adress, unsigned long long> memory;

    std::string line;
    while (std::getline(std::cin, line)) {
        if (line.find("mask") != std::string::npos) {
            std::string newMask = line.substr(line.find("=") + 2);
            for (size_t i = 0; i < newMask.size(); ++i) {
                if (newMask[i] == 'X') {
                    mask[i] = 2;
                }
                else {
                    mask[i] = newMask[i] - '0';
                }
            }
        }
        else {
            Adress adress = atoi(line.substr(line.find("[") + 1, line.find("]") - line.find("[") - 1).c_str());
            unsigned long long value = atoll(line.substr(line.find("=") + 2).c_str());
            for (size_t i = 0; i < 36; ++i) {
                if (mask[i] != 2) {
                    if ((value & (unsigned long long) 1 << (35 - i)) != 0)
                        value -= (unsigned long long) 1 << (35 - i);
                    value |= mask[i] << (35 - i);
                }
            }
            memory[adress] = value;
        }
    }

    unsigned long long sum = 0;
    for (auto val : memory) {
        sum += val.second;
    }

    std::cout << sum << std::endl;

    return 0;
}