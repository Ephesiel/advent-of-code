#include <iostream>
#include <map>

typedef unsigned long long Adress;

void writeAdresses(std::map<Adress, unsigned long long>& memory, unsigned long long* mask, size_t actualBit, Adress adress, unsigned long long value) {
    if (actualBit == 36) {
        memory[adress] = value;
    }
    else {
        writeAdresses(memory, mask, actualBit + 1, adress, value);
        if (mask[actualBit] == 2) {
            unsigned long long power = (unsigned long long) 1 << (35 - actualBit);
            if (adress & power) {
                adress -= power;
            }
            else {
                adress += power;
            }
            writeAdresses(memory, mask, actualBit + 1, adress, value);
        }
    }
}

int main(int argc, char** argv) {
    unsigned long long mask[36];
    std::map<Adress, unsigned long long> memory;

    std::string line;
    while (std::getline(std::cin, line)) {
        if (line.find("mask") != std::string::npos) {
            std::string newMask = line.substr(line.find("=") + 2);
            for (size_t i = 0; i < newMask.size(); ++i) {
                if (newMask[i] == 'X') {
                    mask[i] = 2;
                }
                else {
                    mask[i] = newMask[i] - '0';
                }
            }
        }
        else {
            Adress adress = atoi(line.substr(line.find("[") + 1, line.find("]") - line.find("[") - 1).c_str());
            unsigned long long value = atoll(line.substr(line.find("=") + 2).c_str());
            for (size_t i = 0; i < 36; ++i) {
                if (mask[i] != 2) {
                    adress |= mask[i] << (35 - i);
                }
            }
            writeAdresses(memory, mask, 0, adress, value);
        }
    }

    unsigned long long sum = 0;
    for (auto val : memory) {
        sum += val.second;
    }

    std::cout << sum << std::endl;

    return 0;
}