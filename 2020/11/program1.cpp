#include <iostream>
#include <vector>

size_t numberNeighbours(const std::vector<std::string>& seats, size_t x, size_t y, char val) {
    size_t count = 0;
    for (size_t i = x - (x != 0); i <= x + (x < (seats.size() - 1)); ++i) {
        for (size_t j = y - (y != 0); j <= y + (y < (seats[x].size() - 1)); ++j) {
            if ((i != x || j != y) && seats[i][j] == val) {
                count++;
            }
        }
    }
    return count;
}

bool next(std::vector<std::string>& seats) {
    bool changed = false;
    const std::vector<std::string> cp = seats;
    for (size_t i = 0; i < seats.size(); ++i) {
        for (size_t j = 0; j < seats[i].size(); ++j) {
            if (cp[i][j] == 'L' && numberNeighbours(cp, i, j, '#') == 0) {
                seats[i][j] = '#';
                changed = true;
            }
            else if (cp[i][j] == '#' && numberNeighbours(cp, i, j, '#') >= 4) {
                seats[i][j] = 'L';
                changed = true;
            }
        }
    }
    return changed;
}

int main(int argc, char** argv) {
    std::string line;
    std::vector<std::string> seats;

    while (std::getline(std::cin, line)) {
        seats.push_back(line);
    }

    while (next(seats));

    size_t count = 0;
    for (size_t i = 0; i < seats.size(); ++i) {
        for (size_t j = 0; j < seats[i].size(); ++j) {
            if (seats[i][j] == '#') {
                count++;
            }
        }
    }

    std::cout << count << std::endl;

    return 0;
}