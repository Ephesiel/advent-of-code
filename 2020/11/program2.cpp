#include <iostream>
#include <list>
#include <vector>

struct Seat {
    std::list<std::pair<size_t, size_t>> neighbours;
    char type;
};

void findNearestNeighbour(size_t x, size_t y, int moveX, int moveY, std::vector<std::vector<Seat>>& seats) {
    int i = x + moveX;
    int j = y + moveY;
    bool found = false;
    while (!found && i >= 0 && j >= 0 && (size_t) i < seats.size() && (size_t) j < seats[x].size()) {
        if (seats[i][j].type != '.') {
            seats[x][y].neighbours.push_back(std::make_pair(i, j));
            found = true;
        }
        else {
            i += moveX;
            j += moveY;
        }
    }
}

void findNeighbours(std::vector<std::vector<Seat>>& seats, size_t x, size_t y) {
    findNearestNeighbour(x, y, 1, 0, seats);
    findNearestNeighbour(x, y, -1, 0, seats);
    findNearestNeighbour(x, y, 0, 1, seats);
    findNearestNeighbour(x, y, 0, -1, seats);
    findNearestNeighbour(x, y, 1, 1, seats);
    findNearestNeighbour(x, y, 1, -1, seats);
    findNearestNeighbour(x, y, -1, 1, seats);
    findNearestNeighbour(x, y, -1, -1, seats);
}

size_t occupiedNeighbours(const Seat& seat, const std::vector<std::vector<Seat>>& seats) {
    size_t count = 0;
    for (std::pair<size_t, size_t> neighbour : seat.neighbours) {
        count += seats[neighbour.first][neighbour.second].type == '#';
    }
    return count;
}

bool next(std::vector<std::vector<Seat>>& seats) {
    bool changed = false;
    std::vector<std::vector<Seat>> cp = seats;
    
    for (size_t i = 0; i < seats.size(); ++i) {
        for (size_t j = 0; j < seats[i].size(); ++j) {
            if (cp[i][j].type == 'L' && occupiedNeighbours(cp[i][j], cp) == 0) {
                seats[i][j].type = '#';
                changed = true;
            }
            else if (cp[i][j].type == '#' && occupiedNeighbours(cp[i][j], cp) >= 5) {
                seats[i][j].type = 'L';
                changed = true;
            }
        }
    }
    return changed;
}

int main(int argc, char** argv) {
    std::string line;
    std::vector<std::vector<Seat>> seats;

    while (std::getline(std::cin, line)) {
        std::vector<Seat> row(line.size());
        for (size_t i = 0; i < line.size(); ++i) {
            row[i].type = line[i];
        }
        seats.push_back(row);
    }

    for (size_t i = 0; i < seats.size(); ++i) {
        for (size_t j = 0; j < seats[i].size(); ++j) {
            if (seats[i][j].type != '.') {
                findNeighbours(seats, i, j);
            }
        }
    }

    while (next(seats));

    size_t count = 0;
    for (size_t i = 0; i < seats.size(); ++i) {
        for (size_t j = 0; j < seats[i].size(); ++j) {
            if (seats[i][j].type == '#') {
                count++;
            }
        }
    }

    std::cout << count << std::endl;

    return 0;
}