#include <iostream>
#include <list>
#include <vector>

struct Set {
    size_t inf;
    size_t sup;
};

struct Field {
    std::string name;
    std::list<Set> sets;
    std::vector<bool> possibleValues = std::vector<bool>(20, true);
    int value = -1;
};

struct Ticket {
    std::vector<size_t> numbers;
};

std::vector<size_t> split(const std::string& s, const std::string& del) {
    std::vector<size_t> result;
    size_t start = 0;
    size_t end = s.find(del);

    while (end != std::string::npos) {
        result.push_back(atoi(s.substr(start, end - start).c_str()));
        start = end + del.size();
        end = s.find(del, start);
    }

    result.push_back(atoi(s.substr(start).c_str()));
    return result;
}

Set createSet(const std::string& s) {
    size_t pos = s.find("-");
    Set set;
    set.inf = atoi(s.substr(0, pos).c_str());
    set.sup = atoi(s.substr(pos + 1).c_str());

    return set;
}

Ticket createTicket(const std::string& s) {
    Ticket ticket;
    ticket.numbers = split(s, ",");

    return ticket;
}

Field createField(const std::string& s) {
    size_t pos1 = s.find(":");
    size_t pos2 = s.find(" or ");
    Field field;
    field.name = s.substr(0, pos1);
    field.sets.push_back(createSet(s.substr(pos1 + 1, pos2 - pos1)));
    field.sets.push_back(createSet(s.substr(pos2 + 4)));

    return field;
}

int main(int argc, char** argv) {
    std::list<Field> fields;

    std::string line;
    while (std::getline(std::cin, line) && !line.empty()) {
        fields.push_back(createField(line));
    }

    // Title
    std::getline(std::cin, line);
    // My ticket
    std::getline(std::cin, line);
    Ticket myTicket = createTicket(line);
    // Empty line
    std::getline(std::cin, line);
    // Title
    std::getline(std::cin, line);
    
    std::vector<Ticket> tickets;
    while (std::getline(std::cin, line)) {
        Ticket ticket = createTicket(line);
        bool found = true;
        for (size_t number : ticket.numbers) {
            found = false;
            for (const Field& field : fields) {
                for (const Set& set : field.sets) {
                    if (number >= set.inf && number <= set.sup) {
                        found = true;
                        break;
                    }
                }
                if (found) {
                    break;
                }
            }
            if (!found) {
                break;
            }
        }
        if (found) {
            tickets.push_back(ticket);
        }
    }

    for (const Ticket& ticket : tickets) {
        for (Field& field : fields) {
            for (size_t i = 0; i < ticket.numbers.size(); ++i) {
                if (field.possibleValues[i]) {
                    bool found = false;
                    for (const Set& set : field.sets) {
                        if (ticket.numbers[i] >= set.inf && ticket.numbers[i] <= set.sup) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        field.possibleValues[i] = false;
                    }
                }
            }
        }
    }

    size_t validFields = 0;
    while (validFields != fields.size()) {
        for (Field& field : fields) {
            if (field.value == -1) {
                int value = -1;
                for (size_t i = 0; i < field.possibleValues.size(); ++i) {
                    if (field.possibleValues[i]) {
                        if (value == -1) {
                            value = i;
                        }
                        else {
                            value = -1;
                            break;
                        }
                    }
                }
                if (value != -1) {
                    field.value = value;
                    validFields++;
                    for (Field& field2 : fields) {
                        field2.possibleValues[value] = false;
                    }
                }
            }
        }
    }

    size_t result = 1;
    for (const Field& field : fields) {
        if (field.name.find("departure") != std::string::npos) {
            result *= myTicket.numbers[field.value];
        }
    }

    std::cout << result << std::endl;

    return 0;
}