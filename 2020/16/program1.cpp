#include <iostream>
#include <list>
#include <vector>

struct Set {
    size_t inf;
    size_t sup;
};

struct Field {
    std::string name;
    std::list<Set> sets;
    std::vector<bool> possibleValue = std::vector<bool>(20, true);
};

std::vector<size_t> split(const std::string& s, const std::string& del) {
    std::vector<size_t> result;
    size_t start = 0;
    size_t end = s.find(del);

    while (end != std::string::npos) {
        result.push_back(atoi(s.substr(start, end - start).c_str()));
        start = end + del.size();
        end = s.find(del, start);
    }

    result.push_back(atoi(s.substr(start).c_str()));
    return result;
}

Set createSet(const std::string& s) {
    size_t pos = s.find("-");
    Set set;
    set.inf = atoi(s.substr(0, pos).c_str());
    set.sup = atoi(s.substr(pos + 1).c_str());
    return set;
}

int main(int argc, char** argv) {
    std::list<Field> fields;

    std::string line;
    while (std::getline(std::cin, line) && !line.empty()) {
        size_t pos = line.find(":");
        Field field;
        field.name = line.substr(0, pos);

        line = line.substr(pos + 1);
        pos = line.find(" or ");
        field.sets.push_back(createSet(line.substr(0, pos)));
        field.sets.push_back(createSet(line.substr(pos + 4)));

        fields.push_back(field);
    }

    // Title
    std::getline(std::cin, line);
    // My ticket
    std::getline(std::cin, line);
    // Empty line
    std::getline(std::cin, line);
    // Title
    std::getline(std::cin, line);
    
    size_t sum = 0;
    while (std::getline(std::cin, line)) {
        for (size_t number : split(line, ",")) {
            bool found = false;
            for (const Field& field : fields) {
                for (const Set& set : field.sets) {
                    if (number >= set.inf && number <= set.sup) {
                        found = true;
                        break;
                    }
                }
                if (found) {
                    break;
                }
            }
            if (!found) {
                sum += number;
            }
        }
    }

    std::cout << sum << std::endl;

    return 0;
}