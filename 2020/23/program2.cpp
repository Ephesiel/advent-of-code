#include <iostream>
#include <vector>
#include <algorithm>

typedef size_t Cup;

size_t game(const std::vector<Cup>& originals, size_t range, size_t move) {
    std::vector<Cup> cupsAdj(range + 1);
    for (size_t i = 0; i < originals.size(); ++i) {
        cupsAdj[originals[i]] = originals[(i + 1) % originals.size()];
    }
    
    if (range > originals.size()) {
        cupsAdj[originals.back()] = originals.size() + 1;
        for (size_t i = originals.size() + 1; i < range; ++i) {
            cupsAdj[i] = i + 1;
        }
        cupsAdj[range] = originals.front();
    }

    Cup currentCup = originals[0];
    for (size_t i = 0; i < move; ++i) {
        Cup cup1 = cupsAdj[currentCup];
        Cup cup2 = cupsAdj[cup1];
        Cup cup3 = cupsAdj[cup2];
        Cup destinationCup = currentCup;
        do {
            if (!--destinationCup) {
                destinationCup = range;
            }
        } while (destinationCup == cup1 || destinationCup == cup2 || destinationCup == cup3);

        Cup lastDestinationAdj = cupsAdj[destinationCup];
        cupsAdj[currentCup] = cupsAdj[cup3];
        cupsAdj[destinationCup] = cup1;
        cupsAdj[cup3] = lastDestinationAdj;

        currentCup = cupsAdj[currentCup];
    }

    return cupsAdj[1] * cupsAdj[cupsAdj[1]];
}

int main(int argc, char** argv) {
    std::string line;
    std::getline(std::cin, line);
    std::vector<Cup> cups(9);

    for (size_t i = 0; i < line.size(); ++i) {
        cups[i] = line[i] - '0';
    }

    std::cout << game(cups, 1'000'000, 10'000'000) << std::endl;

    return 0;
}