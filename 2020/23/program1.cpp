#include <iostream>
#include <vector>
#include <algorithm>

typedef size_t Cup;

int main(int argc, char** argv) {
    std::vector<Cup> cups(9);
    std::string line;
    std::getline(std::cin, line);
    for (size_t i = 0; i < line.size(); ++i) {
        cups[i] = line[i] - '0';
    }

    size_t index = 0; 
    for (size_t i = 0; i < 100; ++i) {
        Cup currentCup = cups[index % 9];
        Cup cup1 = cups[(index + 1) % 9];
        Cup cup2 = cups[(index + 2) % 9];
        Cup cup3 = cups[(index + 3) % 9];
        Cup destinationCup = currentCup;
        do {
            if (!--destinationCup) {
                destinationCup = 9;
            }
        } while (destinationCup == cup1 || destinationCup == cup2 || destinationCup == cup3);

        std::vector<Cup> newCups;
        for (size_t j = 0; j < 10; ++j) {
            if (cups[j] != cup1 && cups[j] != cup2 && cups[j] != cup3) {
                newCups.push_back(cups[j]);
            }

            if (cups[j] == currentCup) {
                index = newCups.size();
            }
            
            if (cups[j] == destinationCup) {
                newCups.push_back(cup1);
                newCups.push_back(cup2);
                newCups.push_back(cup3);
            }
        }
        
        cups = newCups;
    }

    index = std::distance(cups.begin(), std::find(cups.begin(), cups.end(), 1));
    for (size_t i = 0; i < line.size() - 1; ++i) {
        line[i] = cups[(i + 1 + index) % 9] + '0';
    }
    line = line.substr(0, line.size() - 1);

    std::cout << line << std::endl;

    return 0;
}