#include <iostream>
#include <map>
#include <vector>
#include <list>
#include <set>

struct Cmd {
    std::string cmd;
    int val;
};

int main(int argc, char** argv) {
    std::string line;
    std::vector<Cmd> cmds;

    while (std::getline(std::cin, line)) {
        size_t pos = line.find(' ');
        Cmd cmd;
        cmd.cmd = line.substr(0, pos);
        cmd.val = atoi(line.substr(pos + 1).c_str());
        cmds.push_back(cmd);
    }

    bool found = false;
    size_t index = 0;
    size_t acc = 0;

    while (!found && index < cmds.size()) {
        std::vector<bool> pass(cmds.size(), false);
        size_t i = 0;
        acc = 0;

        while (index < cmds.size() && cmds[index].cmd == "acc") {
            index++;
        }

        if (index >= cmds.size()) {
            break;
        }

        cmds[index].cmd = cmds[index].cmd == "nop" ? "jmp" : "nop";

        while (i < cmds.size() && ! pass[i]) {
            pass[i] = true;
            Cmd cmd = cmds[i];

            if (cmd.cmd == "nop") {
                i++;
            }
            else if (cmd.cmd == "acc") {
                acc += cmd.val;
                i++;
            }
            else if (cmd.cmd == "jmp") {
                i += cmd.val;
            }
            else {
                std::cerr << "erreur" << std::endl;
                break;
            }
        }

        if (i >= cmds.size()) {
            found = true;
        }

        cmds[index].cmd = cmds[index].cmd == "nop" ? "jmp" : "nop";
        index++;
    }
    
    if (found) {
        std::cout << acc << std::endl;
    }
    else {
        std::cerr << "not found" << std::endl;
    }

    return 0;
}