#include <iostream>
#include <map>
#include <vector>
#include <list>
#include <set>

struct Cmd {
    std::string cmd;
    int val;
};

int main(int argc, char** argv) {
    std::string line;
    std::vector<Cmd> cmds;

    while (std::getline(std::cin, line)) {
        size_t pos = line.find(' ');
        Cmd cmd;
        cmd.cmd = line.substr(0, pos);
        cmd.val = atoi(line.substr(pos + 1).c_str());
        cmds.push_back(cmd);
    }

    std::vector<bool> pass(cmds.size(), false);
    size_t index = 0;
    size_t acc = 0;

    while (! pass[index]) {
        pass[index] = true;
        Cmd cmd = cmds[index];

        if (cmd.cmd == "nop") {
            index++;
        }
        else if (cmd.cmd == "acc") {
            acc += cmd.val;
            index++;
        }
        else if (cmd.cmd == "jmp") {
            index += cmd.val;
        }
        else {
            std::cerr << "erreur" << std::endl;
            break;
        }
    }
    
    std::cout << acc << std::endl;

    return 0;
}