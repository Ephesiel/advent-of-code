#include <iostream>
#include <list>
#include <set>
#include <map>
#include <algorithm>

typedef std::string Alergen;
typedef std::string Ingredient;

struct Food {
    std::list<Alergen> alergens;
    std::list<Ingredient> ingredients;
};

int main(int argc, char** argv) {
    std::list<Food> foods;
    std::set<Alergen> alergens;
    std::set<Ingredient> ingredients;

    std::string line;
    while (std::getline(std::cin, line)) {
        bool a = false;
        Food food;
        while (!line.empty()) {
            if (a) {
                size_t space = line.find(", ");
                if (space == std::string::npos) {
                    Alergen alergen = line.substr(0, line.find(')'));
                    food.alergens.push_back(alergen);
                    alergens.emplace(alergen);
                    line = "";  
                }
                else {
                    Alergen alergen = line.substr(0, space);
                    food.alergens.push_back(alergen);
                    alergens.emplace(alergen);
                    line = line.substr(space + 2);
                }
            }
            else {
                size_t bracket = line.find(" (contains ");
                size_t space = line.find(' ');

                Ingredient ingredient = line.substr(0, space);
                food.ingredients.push_back(ingredient);
                ingredients.emplace(ingredient);
                line = line.substr(space + 1);

                if (bracket == space) {
                    a = true;
                    line = line.substr(10);
                }
            }
        }
        foods.push_back(food);
    }

    std::map<Alergen, std::set<Ingredient>> possibilities;

    for (const Food& food : foods) {
        for (const Alergen& alergen : food.alergens) {
            if (possibilities.count(alergen)) {
                std::set<Ingredient> newSet;
                for (const Ingredient& ingredient1 : possibilities[alergen]) {
                    for (const Ingredient& ingredient2 : food.ingredients) {
                        if (ingredient1 == ingredient2) {
                            newSet.emplace(ingredient1);
                        }
                    }
                }
                possibilities[alergen] = newSet;
            }
            else {
                possibilities[alergen] = std::set<Ingredient>(food.ingredients.begin(), food.ingredients.end());
            }
        }
    }

    std::map<Alergen, Ingredient> results;

    while (possibilities.size()) {
        for (std::map<Alergen, std::set<Ingredient>>::iterator it = possibilities.begin(); it != possibilities.end(); ++it) {
            if (it->second.size() == 1) {
                Alergen alergen = it->first;
                Ingredient ingredient = *it->second.begin();
                results[alergen] = ingredient;
                possibilities.erase(it);

                for (it = possibilities.begin(); it != possibilities.end(); ++it) {
                    std::set<Ingredient>::iterator found = it->second.find(ingredient);
                    if (found != it->second.end()) {
                        it->second.erase(found);
                    }
                }
                break;
            }
        }
    }

    size_t count = 0;
    for (const Food& food : foods) {
        for (const Ingredient& ingredient : food.ingredients) {
            bool found = false;
            for (std::map<Alergen, Ingredient>::iterator it = results.begin(); it != results.end(); ++it) {
                if (it->second == ingredient) {
                    found = true;
                }
            }
            count += !found;
        }
    }
    
    std::cout << count << std::endl;

    return 0;
}