#!/usr/bin/env python3

import time

# input
entries = open("input.txt").readlines()

values = {int(entries[i*12].split()[1].split(':')[0]):tuple(e[:-1] for e in entries[i*12+1:i*12+11]) for i in range(len(entries) // 12 + 1)}
size = 12

# part 1
def get_tile_borders(tile):
    return (tile[0], ''.join(tile[i][-1] for i in range(10)), tile[9], ''.join(tile[i][0] for i in range(10)))

def get_tile_orientations(tile, inverse = False):
    orientations = []
    for i in range(4):
        tile = tuple(''.join(tile[-1 - j][k] for j in range(10)) for k in range(10))
        orientations.append((tile, get_tile_borders(tile)))
    
    if inverse is False:
        tile = tuple(tile[-1 -j] for j in range(10))
        orientations += get_tile_orientations(tile, True)

    return orientations

tiles = [(id, get_tile_orientations(values[id])) for id in values]
checked_tiles = {t[0]:False for t in tiles}
chose_tiles = [[None]*size for i in range(size)]

def check_borders(borders, x, y):
    if x != 0 and chose_tiles[x - 1][y][2][1] != borders[3]:
        return False
    if y != 0 and chose_tiles[x][y - 1][2][2] != borders[0]:
        return False
    return True

def backtrack(n = 0):
    if n == size*size:
        return True
    
    x = n % size
    y = n // size

    for id, orientations in tiles:
        if not checked_tiles[id]:
            for orientation, borders in orientations:
                if check_borders(borders, x, y):
                    checked_tiles[id] = True
                    chose_tiles[x][y] = (id, orientation, borders)
                    if backtrack(n + 1):
                        return True
                    checked_tiles[id] = False
                    chose_tiles[x][y] = None

    return False

def print_tiles():
    for y in range(size):
        print(' '.join(' ' * 3 + str(chose_tiles[x][y][0]) + ' ' * 3 for x in range(size) if chose_tiles[x][y] is not None))
        for l in range(10):
            print(' '.join(chose_tiles[x][y][1][l] if chose_tiles[x][y] is not None else '.' * 10 for x in range(size)))
        print()

backtrack()
print(chose_tiles[0][0][0] * chose_tiles[0][size-1][0] * chose_tiles[size-1][0][0] * chose_tiles[size-1][size-1][0])

# part 2
final_image = []
img_size = size*8

for y in range(size):
    for l in range(1, 9):
        line = ''.join(chose_tiles[x][y][1][l][1:9] for x in range(size))
        final_image.append(line)

all_images = [final_image]
for i in range(3):
    final_image = [''.join(final_image[k][-1 - j] for k in range(img_size)) for j in range(img_size)]
    all_images.append(final_image)

all_images += [[img[-1 - i] for i in range(img_size)] for img in all_images]

dragon_coord = (
    (0, 18),
    (1, 0),
    (1, 5),
    (1, 6),
    (1, 11),
    (1, 12),
    (1, 17),
    (1, 18),
    (1, 19),
    (2, 1),
    (2, 4),
    (2, 7),
    (2, 10),
    (2, 13),
    (2, 16)
)

for img in all_images:
    nb_dragons = 0
    for i in range(img_size - 2):
        for j in range(img_size - 19):
            dragon = 1
            for x, y in dragon_coord:
                dragon &= img[i + x][j + y] in ('#', 'O')

            if dragon:
                nb_dragons += 1
                for x, y in dragon_coord:
                    img[i + x] = ''.join(img[i + x][k] if k != j + y else 'O' for k in range(img_size))
    
    if nb_dragons > 0:
        print('\n'.join(img))
        
        print(sum(img[i][j] == '#' for i in range(img_size) for j in range(img_size)))
        break