#!/usr/bin/env python3

# input
entries = open("input.txt").readlines()

# part 1
player_pos = [int(e[-2]) - 1 for e in entries]
player_score = [0] * 2
nb_rolls = 0
turn = 0

def next_roll():
    global nb_rolls
    r = nb_rolls % 100 + 1
    nb_rolls += 1
    return r

while not player_score[(turn + 1) % 2] >= 1000:
    player_pos[turn % 2] += sum(next_roll() for _ in range(3))
    player_pos[turn % 2] %= 10
    player_score[turn % 2] += player_pos[turn % 2] + 1
    turn += 1

print(player_score[turn % 2] * nb_rolls)

# part 2
M = [[[[[None for _ in range(2)] for _ in range(22)] for _ in range(22)] for _ in range(10)] for _ in range(10)]

def possibilities(space_j1, space_j2, points_j1 = 0, points_j2 = 0, j1_turn = True):
    #print(space_j1, space_j2, points_j1, points_j2)
    if points_j1 >= 21:
        points_j1 = 21
        M[space_j1][space_j2][points_j1][points_j2][j1_turn] = (1, 0)

    if points_j2 >= 21:
        points_j2 = 21
        M[space_j1][space_j2][points_j1][points_j2][j1_turn] = (0, 1)

    if M[space_j1][space_j2][points_j1][points_j2][j1_turn] != None:
        return M[space_j1][space_j2][points_j1][points_j2][j1_turn]
    
    p = [0, 0]
    for roll1 in [1, 2, 3]:
        for roll2 in [1, 2, 3]:
            for roll3 in [1, 2, 3]:
                p0, p1 = 0, 0
                if j1_turn:
                    next_space = (space_j1 + roll1 + roll2 + roll3) % 10
                    next_points = points_j1 + (next_space + 1)
                    p0, p1 = possibilities(next_space, space_j2, next_points, points_j2, False)
                else:
                    next_space = (space_j2 + roll1 + roll2 + roll3) % 10
                    next_points = points_j2 + (next_space + 1)
                    p0, p1 = possibilities(space_j1, next_space, points_j1, next_points, True)
                p[0] += p0
                p[1] += p1
    
    M[space_j1][space_j2][points_j1][points_j2][j1_turn] = (p[0], p[1])
    return (p[0], p[1])

print(max(possibilities(int(entries[0][-2]) - 1, int(entries[1][-2]) - 1)))