#!/usr/bin/env python3

import sys

# input
entries = open("input.txt").readlines()

# part 1
pos = list(map(int, entries[0].split(',')))
m = sys.maxsize

for i in range(max(pos)):
    s = sum([abs(p - i) for p in pos])
    if s < m:
        m = s

print(m)
m = sys.maxsize

# part 2
for i in range(max(pos)):
    s = sum([abs(p - i)*(abs(p - i) + 1)/2 for p in pos])
    if s < m:
        m = s

print(m)