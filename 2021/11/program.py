#!/usr/bin/env python3

import sys

# input
entries = open("input.txt").readlines()

M = [list(map(int, e[:-1])) for e in entries]
flash = 0
flashed = []

def augmente(i, j):
    global flash

    if i >= 0 and i < 10 and j >= 0 and j < 10:
        M[i][j] += 1
        if M[i][j] == 10:
            flash += 1
            flashed.append((i, j))
            augmente(i - 1, j - 1)
            augmente(i - 1, j)
            augmente(i - 1, j + 1)
            augmente(i, j - 1)
            augmente(i, j + 1)
            augmente(i + 1, j - 1)
            augmente(i + 1, j)
            augmente(i + 1, j + 1)

def step():
    global flashed

    for i in range(10):
        for j in range(10):
            augmente(i, j)
    for i, j in flashed:
        M[i][j] = 0
    flashed = []

# part 1
for _ in range(100):
    step()

print(flash)

# part 2
s = 100
while flash != 100:
    s += 1
    flash = 0
    step()

print(s)