#!/usr/bin/env python3

# input
entries = open("input.txt").readlines()

# part 1
fish = list(map(int, entries[0].split(',')))

for i in range(80):
    fish = [8 for f in fish if f == 0] + [6 if f == 0 else f - 1 for f in fish]

print(len(fish))

# part 2
fish = list(map(int, entries[0].split(',')))
nb_fish = [0] * 9
for f in fish:
    nb_fish[f] += 1

for i in range(256):
    t = i % 7
    a = nb_fish[7]
    nb_fish[7] = nb_fish[8]
    nb_fish[8] = nb_fish[t]
    nb_fish[t] += a

print(sum(nb_fish))