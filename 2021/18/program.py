#!/usr/bin/env python3

# input
entries = open("input.txt").readlines()

def sanitize(v):
    if v.isdigit():
        return [int(v)]
    
    opened = 0
    for i in range(1, len(v)):
        if v[i] == ',' and opened == 0:
            v1 = v[1:i]
            v2 = v[i+1:-1]
            return [sanitize(v1), sanitize(v2)]
        elif v[i] == '[':
            opened += 1
        elif v[i] == ']':
            opened -= 1

def modify(x, v, p):
    if len(x) == 1:
        x[0] += v
    else:
        modify(x[p], v, p)

def explode(x, d = 0):
    if len(x) == 1:
        return False
    
    if d == 4:
        return [x[0][0], x[1][0]]

    for i in [0, 1]:
        r = explode(x[i], d + 1)
        o = (i + 1) % 2
        if r is not False:
            if r[o] is not False:
                if r[i] is not False:
                    x[i] = [0]
                modify(x[o], r[o], i)
                r[o] = False
            return r
    
    return False

def split(x):
    if len(x) == 2:
        return split(x[0]) or split(x[1])
    if x[0] >= 10:
        v = x[0]
        x[0] = [v // 2]
        x.append([v - (v // 2)])
        return True
    return False
        
def add(x, y):
    r = [x, y]
    while explode(r) or split(r):
        pass
    return r

def magnitude(x):
    if len(x) == 1:
        return x[0]
    return 3*magnitude(x[0]) + 2*magnitude(x[1])

# part 1
s = sanitize(entries[0][:-1])

for e in entries[1:]:
    s = add(s, sanitize(e[:-1]))

print(magnitude(s))

# part 2
m = 0
for e1 in entries:
    for e2 in entries:
        if e1 != e2:
            r = magnitude(add(sanitize(e1[:-1]), sanitize(e2[:-1])))
            if r > m:
                m = r
print(m)