#!/usr/bin/env python3

from copy import deepcopy
import sys

# input
entries = open("input.txt").readlines()

def sanitize(l, t):
    while len(t) >= 0 and t[-1] == l:
        t = t[:-1]
    return t

highway = [None] * 11
roomA = sanitize('A', [entries[2][3], 'D', 'D', entries[3][3]])
roomB = sanitize('B', [entries[2][5], 'C', 'B', entries[3][5]])
roomC = sanitize('C', [entries[2][7], 'B', 'A', entries[3][7]])
roomD = sanitize('D', [entries[2][9], 'A', 'C', entries[3][9]])

def can_go(a, b, h, count_a = False):
    if a < b:
        return all(h[i] == None for i in range(a + (not count_a), b + 1))
    return all(h[i] == None for i in range(b, a + count_a))

def price(l, a, b, deep = 0):
    return (deep + abs(b - a)) * {'A':1, 'B':10, 'C':100, 'D':1000}[l]

depth = 4
best_prize = sys.maxsize
possibilities = [(highway, [roomA, roomB, roomC, roomD], 0, [depth - len(r) for r in [roomA, roomB, roomC, roomD]], "\n")]

while len(possibilities) > 0:
    ind, p, s = 0, sys.maxsize, 0
    for i in range(len(possibilities)):
        if sum(possibilities[i][3]) > s or sum(possibilities[i][3]) == s and possibilities[i][2] < p:
            ind, p, s = i, possibilities[i][2], sum(possibilities[i][3])

    h, r, p, g, path = possibilities.pop(i)
    if p >= best_prize:
        continue

    if sum(g) == depth*4:
        best_prize = p
        # Comme la boucle ne s'arrête pas, on peut récupérer le meilleur grâce au print
        # Il faut juste attendre un peu
        print(p, path)
        continue

    for i in range(11):
        if h[i] != None:
            roomId = {'A':0, 'B':1, 'C':2, 'D':3}[h[i]]
            x = {'A':2, 'B':4, 'C':6, 'D':8}[h[i]]
            if len(r[roomId]) == 0 and can_go(i, x, h):
                new_price = p + price(h[i], i, x, depth - g[roomId])
                new_goods = deepcopy(g)
                new_h = deepcopy(h)
                new_goods[roomId] += 1
                new_h[i] = None
                possibilities.append((new_h, r, new_price, new_goods, path + f"{h[i]} highway {i} -> room depth {depth - g[roomId]}, price : {new_price}.\n"))
    
    for i in range(len(r)):
        if len(r[i]) > 0:
            x = {0:2, 1:4, 2:6, 3:8}[i]
            for y in [0, 1, 3, 5, 7, 9, 10]:
                if can_go(x, y, h, True):
                    new_price = p + price(r[i][0], x, y, depth + 1 - len(r[i]) - g[i])
                    new_rooms = deepcopy(r)
                    new_h = deepcopy(h)
                    new_h[y] = new_rooms[i].pop(0)
                    possibilities.append((new_h, new_rooms, new_price, g, path + f"{r[i][0]} go from room {i} depth {depth + 1 - len(r[i]) - g[i]} -> highway {y}, price : {new_price}.\n"))

print(best_prize)