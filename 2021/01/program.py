#!/usr/bin/env python3

# input
values = [int(v) for v in open("input.txt")]

# part 1
print(sum([values[i] < values[i + 1] for i in range(len(values) - 1)]))

# part 2
print(sum([values[i] < values[i + 3] for i in range(len(values) - 3)]))
