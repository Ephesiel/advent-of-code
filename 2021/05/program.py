#!/usr/bin/env python3

import re 

# input
entries = open("input.txt").readlines()

coords = [tuple(map(int, re.findall('\d+', e))) for e in entries]
maxx = max([max(c[0], c[2]) for c in coords])
maxy = max([max(c[1], c[3]) for c in coords])

m = [[0] * (maxy + 1) for _ in range(maxx + 1)]
d = [[0] * (maxy + 1) for _ in range(maxx + 1)]

for c in coords:
    if c[0] == c[2] or c[1] == c[3]:
        for x in range(min(c[0], c[2]), max(c[0], c[2]) + 1):
            for y in range(min(c[1], c[3]), max(c[1], c[3]) + 1):
                m[x][y] += 1
                d[x][y] += 1
    else:
        x = c[0]
        y = c[1]
        dx = -1 if c[0] > c[2] else 1
        dy = -1 if c[1] > c[3] else 1
        for _ in range(abs(c[0] - c[2]) + 1):
            d[x][y] += 1
            x += dx
            y += dy

print(sum([x > 1 for l in m for x in l]))
print(sum([x > 1 for l in d for x in l]))