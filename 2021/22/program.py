#!/usr/bin/env python3

from time import time

# input
entries = open("input.txt").readlines()

cuboids = []
for e in entries:
    on, c = e.split(' ')
    x, y, z = map(lambda x: tuple(map(int, x.split('=')[1].split('..'))), c.split(','))
    cuboids.append((on == 'on', x, y, z))

# part 1
motor = [[[False] * 101 for _ in range(101)] for _ in range(101)]
for cuboid in cuboids:
    if cuboid[1][0] >= -50 and cuboid[1][1] <= 50 and cuboid[2][0] >= -50 and cuboid[2][1] <= 50 and cuboid[3][0] >= -50 and cuboid[3][1] <= 50:
        for x in range(cuboid[1][0], cuboid[1][1] + 1):
            for y in range(cuboid[2][0], cuboid[2][1] + 1):
                for z in range(cuboid[3][0], cuboid[3][1] + 1):
                    motor[x + 50][y + 50][z + 50] = cuboid[0]

print(sum(sum(l) for p in motor for l in p))

# part 2
def intersection_segment(min1, max1, min2, max2):
    if min1 <= min2 <= max1 <= max2: return (min2, max1)
    if min2 <= min1 <= max2 <= max1: return (min1, max2)
    if min1 <= min2 <= max2 <= max1: return (min2, max2)
    if min2 <= min1 <= max1 <= max2: return (min1, max1)
    return None

def intersection(cuboid1, cuboid2, on):
    inter_x = intersection_segment(cuboid1[1][0], cuboid1[1][1], cuboid2[1][0], cuboid2[1][1])
    inter_y = intersection_segment(cuboid1[2][0], cuboid1[2][1], cuboid2[2][0], cuboid2[2][1])
    inter_z = intersection_segment(cuboid1[3][0], cuboid1[3][1], cuboid2[3][0], cuboid2[3][1])
    
    if inter_x is not None and inter_y is not None and inter_z is not None:
        return (on, inter_x, inter_y, inter_z)
    return None

def volume(cuboid):
    return (cuboid[1][1] - cuboid[1][0] + 1) * (cuboid[2][1] - cuboid[2][0] + 1) * (cuboid[3][1] - cuboid[3][0] + 1)

def value(cuboid):
    return volume(cuboid) * (1 if cuboid[0] else -1)

def addCuboid(cuboid, passed_cuboids):
    new_cuboids = []
    if cuboid[0]:
        new_cuboids.append(cuboid)
    for c in passed_cuboids:
        inter = intersection(c, cuboid, not c[0])
        if inter is not None:
            new_cuboids.append(inter)

    return passed_cuboids + new_cuboids

t = time()
passed_cuboids = []
for cuboid in cuboids:
    passed_cuboids = addCuboid(cuboid, passed_cuboids)

print(sum(value(c) for c in passed_cuboids))
print(time() - t, "s")