#!/usr/bin/env python3

from copy import deepcopy

# input
entries = open("input.txt").readlines()

M = [[x for x in e[:-1]] for e in entries]
n = len(M[0])
m = len(M)

moved = True
i = 0

while (moved):
    moved = False
    MN = deepcopy(M)
    for x in range(n):
        for y in range(m):
            nx = (x + 1) % n
            if M[y][x] == '>' and M[y][nx] == '.':
                MN[y][x] = '.'
                MN[y][nx] = '>'
                moved = True
    
    M = MN
    MN = deepcopy(M)

    for x in range(n):
        for y in range(m):
            ny = (y + 1) % m
            if M[y][x] == 'v' and M[ny][x] == '.':
                MN[y][x] = '.'
                MN[ny][x] = 'v'
                moved = True
    
    M = MN
    i += 1

print(i)