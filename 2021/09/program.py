#!/usr/bin/env python3

import sys
from math import prod

# input
entries = open("input.txt").readlines()

# part 1
n = len(entries)
M = [list(map(int, l.split()[0])) for l in entries]

print(sum([M[i][j] + 1 for i in range(n) for j in range(n) if
    (i == 0 or M[i - 1][j] > M[i][j]) and
    (i == n - 1 or M[i + 1][j] > M[i][j]) and
    (j == 0 or M[i][j - 1] > M[i][j]) and
    (j == n - 1 or M[i][j + 1] > M[i][j])
]))

# part 2
def bassin(i, j):
    if i >= 0 and i < n and j >= 0 and j < n and M[i][j] != 9:
        M[i][j] = 9
        return 1 + bassin(i - 1, j) + bassin(i + 1, j) + bassin(i, j - 1) + bassin(i, j + 1)
    return 0

bassins = []

for i in range(n):
    for j in range(n):
        if M[i][j] != 9:
            bassins.append(bassin(i, j))

print(prod(sorted(bassins)[-3:]))