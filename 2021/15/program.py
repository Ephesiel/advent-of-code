#!/usr/bin/env python3

import sys

# input
entries = open("input.txt").readlines()

G = [list(map(int, e[:-1])) for e in entries]

n = len(G)
m = len(G[0])

for l in G:
    for i in range(4*m):
        l.append(1 if l[i] == 9 else l[i] + 1)
for i in range(4*n):
    G.append([1 if x == 9 else x + 1 for x in G[i]])

n *= 5
m *= 5

closed = {}
opened = {}
opened[(0, 0)] = 0

def voisins(x, y):
    return [c for c in [(x - 1, y), (x + 1, y), (x, y + 1), (x, y - 1)] if c[0] >= 0 and c[0] < n and c[1] >= 0 and c[1] < m]

while len(opened) > 0:
    d, v = sys.maxsize, None
    for k in opened:
        if opened[k] < d:
            d = opened[k]
            v = k

    del opened[v]
    closed[v] = d
    
    if v[0] == n - 1 and v[1] == m - 1:
        print(d)
        break

    for voisin in voisins(v[0], v[1]):
        new_dist = d + G[voisin[0]][voisin[1]]
        if not voisin in closed and (not voisin in opened or opened[voisin] > new_dist):
            opened[voisin] = new_dist
    