#!/usr/bin/env python3

# input
entries = open("input.txt").readlines()

# Création d'une map où chaque valeur du bingo est associée au tour sur lequel elle tombe
tokens = list(map(int, entries[0].split(',')))
turns = {tokens[i]:i for i in range(len(tokens))}

n = 5
boards = [[list(map(int, e.split())) for e in entries[i:i+n]] for i in range(2, len(entries), n+1)]

winner_turn = len(tokens)
winner = None
loser_turn = 0
loser = None

for b in boards:
    b_lin_winner_turn = min([max([turns[x] for x in l]) for l in b])
    b_col_winner_turn = min([max([turns[l[i]] for l in b]) for i in range(n)])
    b_winner_turn = min(b_col_winner_turn, b_lin_winner_turn)
    
    if (b_winner_turn < winner_turn):
        winner_turn = b_winner_turn
        winner = b
    if (b_winner_turn > loser_turn):
        loser_turn = b_winner_turn
        loser = b

winner_score = sum(winner[i][j] for i in range(n) for j in range(n) if turns[winner[i][j]] > winner_turn)
loser_score = sum(loser[i][j] for i in range(n) for j in range(n) if turns[loser[i][j]] > loser_turn)

print(tokens[winner_turn] * winner_score)
print(tokens[loser_turn] * loser_score)