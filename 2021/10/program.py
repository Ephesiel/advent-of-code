#!/usr/bin/env python3

import sys

# input
entries = open("input.txt").readlines()

# part 1
balises = {'(':')', '[':']', '{':'}', '<':'>'}
points = {')':3, ']':57, '}':1197, '>':25137, '(': 1, '[': 2, '{':3, '<':4}
S = 0
V = []

for e in entries:
    chunks = []
    corrupted = False

    for c in e[:-1]:
        if c in balises:
            chunks.append(c)
        elif len(chunks) > 0 and c == balises[chunks[-1]]:
            chunks.pop(-1)
        else:
            S += points[c]
            corrupted = True
            break
    
    if not corrupted:
        S2 = 0
        for c in reversed(chunks):
            S2 *= 5
            S2 += points[c]
        V.append(S2)

V.sort()
print(S)
print(V[len(V)//2])