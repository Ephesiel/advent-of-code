#!/usr/bin/env python3

import sys
from copy import deepcopy

# input
entries = open("input.txt").readlines()

# part 1
G = {}

def add(a, b):
    if not a in G:
        G[a] = []
    G[a].append(b)

def caps(a):
    return all(map(str.isupper, a))

for e in entries:
    a, b = e[:-1].split('-')
    add(a, b)
    add(b, a)

def chemins(point, chemin, double_visit):
    chemin.append(point)

    if point == 'end':
        return [chemin]

    if not caps(point) and len(chemin) > 1 and point in chemin[:-1]:
            if double_visit or point == 'start':
                return []
            double_visit = True
    
    n = []
    for next_point in G[point]:
        for c in chemins(next_point, chemin[::], double_visit):
            n.append(c)
    
    return n

print(len(chemins('start', [], True)))
print(len(chemins('start', [], False)))