#!/usr/bin/env python3

from math import prod

# input
entries = open("input.txt").readlines()
versions = 0

def hexa_to_bits(hexa):
    return bin(int('1' + hexa, base=16))[3:]

def bits_to_int(bits):
    return int(bits, base=2)

def read_value(p):
    stop = False
    bits = ''
    while not stop:
        if p[0] == '0': stop = True
        bits += p[1:5]
        p = p[5:]

    return (bits_to_int(bits), p)

def read_operation(p, t):
    i = 12 if p[0] == '1' else 16
    l = bits_to_int(p[1:i])
    p = p[i:]

    values = []
    val = 0

    if i == 16:
        b, p = p[:l], p[l:]
        while len(b) > 0:
            v, b = read_packet(b)
            values.append(v)
    else:
        for _ in range(l):
            v, p = read_packet(p)
            values.append(v)
    
    if t == 0:
        val = sum(values)
    elif t == 1:
        val = prod(values)
    elif t == 2:
        val = min(values)
    elif t == 3:
        val = max(values)
    elif t == 5:
        val = 1 if values[0] > values[1] else 0
    elif t == 6:
        val = 1 if values[0] < values[1] else 0
    elif t == 7:
        val = 1 if values[0] == values[1] else 0
    
    return val, p

def read_packet(p):
    global versions
    ver = bits_to_int(p[:3])
    typ = bits_to_int(p[3:6])
    versions += ver

    if typ == 4:
        return read_value(p[6:])
    
    return read_operation(p[6:], typ)
    
v, p = read_packet(hexa_to_bits(entries[0]))
print(versions)
print(v)