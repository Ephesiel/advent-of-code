#!/usr/bin/env python3

# input
entries = open("input.txt").readlines()

# part 1
m = len(entries[0]) - 1
n = [0] * m
for e in entries:
    n = [n[i] + int(e[i]) for i in range(m)]
g = sum([(n[m - i - 1] > len(entries)/2) * 2**i for i in range(m)])
e = ~g & (2**m-1)

print(g*e)

# part 2
def a(l, op, i = 0):
    if len(l) == 1: return l[0]
    b = (sum([int(x[i]) for x in l]) >= len(l)/2) ^ op
    return a([x for x in l if int(x[i]) == b], op, i + 1)

print(int(a(entries, 1), 2) * int(a(entries, 0), 2))