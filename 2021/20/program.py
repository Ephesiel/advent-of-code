#!/usr/bin/env python3

# input
entries = open("input.txt").readlines()

image_enhancement_algorithm = entries[0]
input_image = [e[:-1] for e in entries[2:]]
n = len(input_image[0])
m = len(input_image)

def convert_to_int(s):
    return sum(2**(len(s) - i - 1) for i in range(len(s)) if s[i] == '#')

def get_pixel(x, y, t):
    return image_enhancement_algorithm[convert_to_int(''.join(input_image[i][j] if 0 <= i < n and 0 <= j < m else '#' if t & 1 else '.' for i in range(x - 1, x + 2) for j in range(y - 1, y + 2)))]

def next_image(t):
    global input_image
    global n
    global m

    output_image = [''.join(get_pixel(x, y, t) for y in range(-1, m + 1)) for x in range(-1, n + 1)]
    
    n += 2
    m += 2
    input_image = output_image

nb_tour = 50
for t in range(nb_tour):
    next_image(t)

print(sum(input_image[i][j] == '#' for i in range(n) for j in range(m)))