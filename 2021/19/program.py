#!/usr/bin/env python3

from time import time

# input
entries = open("input.txt").readlines()

scanners = []
v = set()
for e in entries:
    if e == '\n':
        scanners.append((len(scanners), v))
        v = set()
    elif e[1] != '-':
        v.add(tuple(map(int, e.split(','))))

scanners.append((len(scanners), v))

def all_orientations():
    x, y, z, n, p = 0, 1, 2, -1, 1
    return [
        # Tête vers le haut
        [(x, p), (y, p), (z, p)], #  x  y  z
        [(y, n), (x, p), (z, p)], # -y  x  z
        [(x, n), (y, n), (z, p)], # -x -y  z
        [(y, p), (x, n), (z, p)], #  y -x  z
        
        # Tête vers le bas
        [(y, p), (x, p), (z, n)], #  y  x -z
        [(x, p), (y, n), (z, n)], #  x -y -z
        [(y, n), (x, n), (z, n)], # -y -x -z
        [(x, n), (y, p), (z, n)], # -x  y -z

        # Tête vers l'avant
        [(x, p), (z, n), (y, p)], #  x -z  y
        [(z, p), (x, p), (y, p)], #  z  x  y
        [(x, n), (z, p), (y, p)], # -x  z  y
        [(z, n), (x, n), (y, p)], # -z -x  y

        # Tête vers l'arrière
        [(z, n), (x, p), (y, n)], #  -z x -y
        [(x, p), (z, p), (y, n)], #  x  z -y
        [(z, p), (x, n), (y, n)], #  z -x -y
        [(x, n), (z, n), (y, n)], # -x -z -y

        # Tête vers la droite
        [(y, n), (z, n), (x, p)], # -y -z  x
        [(z, p), (y, n), (x, p)], #  z -y  x
        [(y, p), (z, p), (x, p)], #  y  z  x
        [(z, n), (y, p), (x, p)], # -z  y  x

        # Tête vers la gauche
        [(z, n), (y, n), (x, n)], # -z -y -x
        [(y, n), (z, p), (x, n)], # -y  z -x
        [(z, p), (y, p), (x, n)], #  z  y -x
        [(y, p), (z, n), (x, n)], #  y -z -x
    ]

def convert_orientation_to_string(o):
    return ('-' if o[1] == -1 else '') + chr(ord('x') + o[0])

def convert_beacons_to_absolute_position(beacon_positions, scanner_absolute_position, scanner_orientation):
    absolute_position = set()
    for p in beacon_positions:
        absolute_position.add((
            scanner_absolute_position[0] + p[scanner_orientation[0][0]] * scanner_orientation[0][1],
            scanner_absolute_position[1] + p[scanner_orientation[1][0]] * scanner_orientation[1][1],
            scanner_absolute_position[2] + p[scanner_orientation[2][0]] * scanner_orientation[2][1]
        ))
    return absolute_position

def get_scanner_absolute_position(beacon_absolute_position, beacon_position, scanner_orientation):
    return (
        beacon_absolute_position[0] - beacon_position[scanner_orientation[0][0]] * scanner_orientation[0][1],
        beacon_absolute_position[1] - beacon_position[scanner_orientation[1][0]] * scanner_orientation[1][1],
        beacon_absolute_position[2] - beacon_position[scanner_orientation[2][0]] * scanner_orientation[2][1]
    )

def intersection(absolute_positions1, absolute_positions2):
    return set(absolute_positions1 & absolute_positions2)

def can_add_set_to_set(set_of_set, set_to_add):
    for s, _ in set_of_set:
        if len(intersection(s, set_to_add)) == len(s):
            return False
    return True

def can_pair_scanner_with_orientation(absolute_beacon_positions, beacon_positions, scanner_orientation):
    possible_positions = []
    for p1 in beacon_positions:
        for p2 in absolute_beacon_positions:
            scanner_absolute_position = get_scanner_absolute_position(p2, p1, scanner_orientation)
            absolute_positions = convert_beacons_to_absolute_position(beacon_positions, scanner_absolute_position, scanner_orientation)
            if len(intersection(absolute_beacon_positions, absolute_positions)) >= 12 and can_add_set_to_set(possible_positions, absolute_positions):
                possible_positions.append((absolute_positions, scanner_absolute_position))
    
    return possible_positions

def backtrack(all_absolute_beacon_positions, possible_scanners, scanner_positions = [(0, 0, 0)], scanner_orientations = [[(0, 1), (1, 1), (2, 1)]], pairs = []):
    print(len(all_absolute_beacon_positions), '/', len(all_absolute_beacon_positions) + len(possible_scanners))
    if len(possible_scanners) == 0:
        return (all_absolute_beacon_positions, scanner_positions, scanner_orientations, pairs)

    for i in range(len(possible_scanners)):
        for p in all_absolute_beacon_positions:
            for o in all_orientations():
                possible_absolute_positions = can_pair_scanner_with_orientation(p[1], possible_scanners[i][1], o)
                for absolute_positions, scanner_absolute_position in possible_absolute_positions:
                    num1 = possible_scanners[i][0]
                    num2 = p[0]
                    new_possible_scanners = possible_scanners[:]
                    new_possible_scanners.pop(i)
                    result = backtrack(all_absolute_beacon_positions + [(num1, absolute_positions)], new_possible_scanners, scanner_positions + [scanner_absolute_position], scanner_orientations + [o], pairs + [num2])
                    if result is not False:
                        return result
    
    return False

def manhattan(a, b):
    return abs(a[0] - b[0]) + abs(a[1] - b[1]) + abs(a[2] - b[2])

t = time()

a = set()
all_absolute_beacon_positions, scanner_positions, scanner_orientations, pairs = backtrack([scanners[0]], scanners[1:])
for n, s in all_absolute_beacon_positions:
    a = a.union(s)

distMax = 0
for i in range(len(scanners)):
    for j in range(i + 1, len(scanners)):
        dist = manhattan(scanner_positions[i], scanner_positions[j])
        if (dist > distMax):
            distMax = dist

datas = {}
for i in range(1, len(scanners)):
    num = all_absolute_beacon_positions[i][0]
    datas[num] = (pairs[i - 1], scanner_positions[i], scanner_orientations[i])
    
for k in sorted(datas.keys()):
    v = datas[k]
    print(f"scanner {k} : pairé avec scanner {v[0]}. Orientation : {', '.join(convert_orientation_to_string(o) for o in v[2])}. Position absolue : {v[1]}.")

print("\nresult 1 :", len(a))
print("result 2 :", distMax)
print("\ntemps passé : ", time() - t, "s")