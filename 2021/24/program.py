#!/usr/bin/env python3

from time import time

# input
entries = open("input.txt").readlines()

t = time()
all_z = {0: [0, 0]}
for i in range(14):
    div_z = int(entries[18*i + 4].split(' ')[2])
    add_x = int(entries[18*i + 5].split(' ')[2])
    add_y = int(entries[18*i + 15].split(' ')[2])
    new_z = {}
    for z, (m, M) in all_z.items():
        for w in range(1, 10):
            x = (z % 26 + add_x) != w
            nz = z // div_z
            if x:
                nz = nz * 26 + w + add_y
            
            mn = m * 10 + w
            mx = M * 10 + w
            if not nz in new_z:
                new_z[nz] = [mn, mx]
            else:
                if mn < new_z[nz][0]: new_z[nz][0] = mn
                if mx > new_z[nz][1]: new_z[nz][1] = mx

    all_z = new_z
    print(f"Programme n°{i + 1}, possibilités : {len(all_z)}, temps passé {time() - t} s")

print(all_z[0])