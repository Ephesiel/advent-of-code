#!/usr/bin/env python3

import sys

# input
entries = open("input.txt").readlines()

# part 1
print(sum([len(m) in [2, 3, 4, 7] for l in entries for m in l.split('|')[1].split()]))

# part 2
S = 0
for l in entries:
    n, d = l.split('|')
    n = n.split()
    d = d.split()

    v = [None] * 10
    v[1] = [m for m in n if len(m) == 2][0]
    v[4] = [m for m in n if len(m) == 4][0]
    v[7] = [m for m in n if len(m) == 3][0]
    v[8] = [m for m in n if len(m) == 7][0]

    possible_069 = [m for m in n if len(m) == 6]
    possible_235 = [m for m in n if len(m) == 5]

    v[9] = [m for m in possible_069 if all([c in m for c in v[4]])][0]
    possible_069.remove(v[9])
    v[0] = [m for m in possible_069 if all([c in m for c in v[1]])][0]
    possible_069.remove(v[0])
    v[6] = possible_069[0]

    v[5] = [m for m in possible_235 if all([c in v[6] for c in m])][0]
    possible_235.remove(v[5])
    v[3] = [m for m in possible_235 if all([c in v[9] for c in m])][0]
    possible_235.remove(v[3])
    v[2] = possible_235[0]

    S += sum([i*10**(len(d) - j - 1) for j in range(len(d)) for i in range(10) if len(v[i]) == len(d[j]) and all(c in v[i] for c in d[j])])

print(S)