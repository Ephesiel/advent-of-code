#!/usr/bin/env python3

import sys

# input
entries = open("input.txt").readlines()

fold = False
folds = []
dots = set()
for e in entries:
    if e == '\n':
        fold = True
    elif fold:
        coord, value = e.split('=')
        folds.append((coord[-1], int(value)))
    else:
        x, y = map(int, e.split(','))
        dots.add((x, y))

for f in folds:
    c = f[0] == 'y'
    v = f[1]
    next_dots = set()
    for d in dots:
        if d[c] < v:
            next_dots.add(d)
        elif d[c] > v:
            nv = d[c] - (d[c] - v) * 2
            if nv >= 0:
                next_dots.add((d[0], nv) if c else (nv, d[1]))
    dots = next_dots

n = max(d[0] for d in dots) + 1
m = max(d[1] for d in dots) + 1
G = [['.'] * n for _ in range(m)]

for d in dots:
    G[d[1]][d[0]] = '#'

for l in G:
    print(''.join(l))