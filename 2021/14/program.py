#!/usr/bin/env python3

import sys

# input
entries = open("input.txt").readlines()

mol = entries[0][:-1]

genere = {}
count = {}
for e in entries[2:]:
    k, v = e.split(' -> ')
    genere[k] = (k[0] + v[0], v[0] + k[1])
    count[k] = 0

for i in range(len(mol) - 1):
    count[mol[i:i+2]] += 1

next_count = {}
for _ in range(40):
    next_count = {k:0 for k in count}
    for k in count:
        next_count[genere[k][0]] += count[k]
        next_count[genere[k][1]] += count[k]
    count = next_count

nb = {}
for v in count:
    if not v[0] in nb:
        nb[v[0]] = 0
    nb[v[0]] += count[v]

nb[mol[-1]] += 1

print(max(nb.values()) - min(nb.values()))