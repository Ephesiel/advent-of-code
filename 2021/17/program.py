#!/usr/bin/env python3

# input
entries = open("input.txt").readlines()
(xmin, xmax), (ymax, ymin) = map(lambda s:map(lambda x:abs(int(x)), s.split('=')[1].split('..')), entries[0].split(': ')[1].split(', '))

# part 1
print(ymax*(ymax-1)//2)

# part 2
def touch(speed_x, speed_y):
    x,y = 0,0
    while x <= xmax and y <= ymax:
        if xmin <= x <= xmax and ymin <= y <= ymax:
            return True

        x += speed_x
        y += speed_y
        speed_y += 1
        speed_x += -1 if speed_x > 0 else (1 if speed_x < 0 else 0)
    return False

print(sum(touch(x, y) for x in range(xmax + 1) for y in range(-ymax-1, ymax+1)))