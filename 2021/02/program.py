#!/usr/bin/env python3

# input
entries = open("input.txt").readlines()

# part 1
depth = 0
horizontal = 0

for entry in entries:
    t, v = entry.split(' ')
    if t == "forward": horizontal += int(v)
    if t == "down": depth += int(v)
    if t == "up": depth -= int(v)

print(depth * horizontal)

# part 2
depth = 0
horizontal = 0
aim = 0
for entry in entries:
    t, v = entry.split(' ')
    if t == "forward":
        horizontal += int(v)
        depth += int(v) * aim
    if t == "down": aim += int(v)
    if t == "up": aim -= int(v)

print(depth * horizontal)
    
