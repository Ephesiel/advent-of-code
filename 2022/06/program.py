#!/usr/bin/env python3

# input
V = [v[:-1] for v in open("input.txt")][0]

# part 1
print([i for i in range(4,len(V)) if 4 == len(set(V[i-4:i]))][0])

# part 2
print([i for i in range(14,len(V)) if 14 == len(set(V[i-14:i]))][0])