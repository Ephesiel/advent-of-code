#!/usr/bin/env python3

import sys
import math

# input
V = [v[:-1] for v in open("input.txt")][0]
n = len(V)

line = ((0,0), (1,0), (2,0), (3,0))
cross = ((0,1), (1,0), (1,1), (1,2), (2,1))
corner = ((0,0), (1,0), (2,0), (2,1), (2,2))
bar = ((0,0), (0,1), (0,2), (0,3))
square = ((0,0), (0,1), (1,0), (1,1))

forms = (line, cross, corner, bar, square)

def can_go_down(form, attach_point, grid):
    for x, y in form:
        if '#' == grid[attach_point[1] + y - 1][attach_point[0] + x]:
            return False
    return True

def can_go_left(form, attach_point, grid):
    for x, y in form:
        if '#' == grid[attach_point[1] + y][attach_point[0] + x - 1]:
            return False
    return True

def can_go_right(form, attach_point, grid):
    for x, y in form:
        if '#' == grid[attach_point[1] + y][attach_point[0] + x + 1]:
            return False
    return True

def simulate(nb_turns):
    grid = [['#'] * 9]
    highest_rock = 0
    grid_height = 1
    wind = 0

    for i in range(nb_turns):
        for _ in range(highest_rock + 8 - grid_height):
            grid.append(['#'] + ['.'] * 7 + ['#'])
            grid_height += 1

        form = forms[i % 5]
        attach_point = [3, highest_rock + 4]
        down = False

        while True:
            if down:
                if can_go_down(form, attach_point, grid):
                    attach_point[1] -= 1
                else:
                    break
            elif '<' == V[wind % n]:
                if can_go_left(form, attach_point, grid):
                    attach_point[0] -= 1
                wind += 1
            else:
                if can_go_right(form, attach_point, grid):
                    attach_point[0] += 1
                wind += 1
            
            down = not down

        for x, y in form:
            grid[attach_point[1] + y][attach_point[0] + x] = '#'
            if attach_point[1] + y > highest_rock:
                highest_rock = attach_point[1] + y
    
    return highest_rock, grid

def calculate_pattern():
    highest_rock, grid = simulate(2000)
    
    p = [[] for _ in range(highest_rock - 20)]
    for i in range(highest_rock):
        for j in range(i + 1, highest_rock - 20):
            pattern = True
            for k in range(20):
                if grid[i + k] != grid[j + k]:
                    pattern = False
                    break

            if pattern:
                nb_rock_loop = 0

                for k in range(i, j):
                    for x in range(1, 8):
                        nb_rock_loop += '#' == grid[k][x]
                
                nb_block_pattern = (nb_rock_loop // 22) * 5
                return (j-i, nb_block_pattern)

# part 1
print(simulate(2022)[0])

# part 2
size_pattern, nb_block_pattern = calculate_pattern()
N = (1000000 * 1000000)
print(simulate(N % nb_block_pattern + nb_block_pattern*3)[0] + size_pattern * ((N // nb_block_pattern) - 3))