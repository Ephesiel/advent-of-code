#!/usr/bin/env python3

# input
V = [v[:-1] for v in open("input.txt")]

# part 1
print(sum(x - (38 if x < 91 else 96) for x in [ord((set(v[:len(v) // 2]) & set(v[len(v) // 2:])).pop()) for v in V]))

# part 2
print(sum(x - (38 if x < 91 else 96) for x in [ord((set(V[i]) & set(V[i+1]) & set(V[i+2])).pop()) for i in range(0, len(V), 3)]))

