#!/usr/bin/env python3

# input
values = [v for v in open("input.txt")] + ['\n']

a = []
s = 0
for v in values:
    if '\n' == v:
        a.append(s)
        s = 0
    else:
        s += int(v)

# part 1
print(max(a))

# part 2
print(sum(sorted(a)[-3:]))