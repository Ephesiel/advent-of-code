#!/usr/bin/env python3

# input
V = [v[:-1] for v in open("input.txt")]

s = 0
for v in V:
    for i in range(len(v)):
        c = v[-i-1]
        x = -1 if '-' == c else -2 if '=' == c else int(c)
        s += x * 5**i

pentimal = []

while s:
    pentimal.append(s % 5)
    s //= 5

pentimal.append(0)

for i in range(len(pentimal)):
    p = pentimal[i]
    if 3 == p:
        pentimal[i] = '='
        j = 1
        while 4 == pentimal[i + j]:
            pentimal[i + j] = 0
            j += 1
        pentimal[i + j] += 1
    elif 4 == p:
        pentimal[i] = '-'
        j = 1
        while 4 == pentimal[i + j]:
            pentimal[i + j] = 0
            j += 1
        pentimal[i + j] += 1
    else:
        pentimal[i] = str(p)

if '0' == pentimal[-1]:
    pentimal = pentimal[:-1]

print(''.join(reversed(pentimal)))