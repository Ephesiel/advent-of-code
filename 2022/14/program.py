#!/usr/bin/env python3

# input
V = [[eval(c) for c in v[:-1].split(' -> ')] for v in open("input.txt")]

maxy = max(max(c[1] for c in v) for v in V) + 2
stepx = 500 - maxy

R = [['.'] * (2 * maxy + 1) for _ in range(maxy)]
R.append(['#'] * (2 * maxy + 1))

for v in V:
    for i in range(len(v) - 1):
        fromx = min(v[i][0], v[i + 1][0])
        fromy = min(v[i][1], v[i + 1][1])
        tox = max(v[i][0], v[i + 1][0]) + 1
        toy = max(v[i][1], v[i + 1][1]) + 1

        for x in range(fromx, tox):
            for y in range(fromy, toy):
                R[y][x - stepx] = '#'

bx = 500 - stepx
by = 0

sx = bx
sy = by

n1 = 0
n2 = 0

part1 = True
part2 = True

while part1 or part2:
    if R[sy + 1][sx] == '.':
        sy += 1
    elif R[sy + 1][sx - 1] == '.':
        sy += 1
        sx -= 1
    elif R[sy + 1][sx + 1] == '.':
        sy += 1
        sx += 1
    else:
        R[sy][sx] = 'o'
        if sy == maxy - 1:
            part1 = False
        if sx == bx and sy == by:
            part2 = False
        elif part1:
            n1 += 1

        n2 += 1
        sx = bx
        sy = by

# visualization
# for r in R:
#     print(''.join(r))

# part 1
print(n1)

# part 2
print(n2)