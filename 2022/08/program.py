#!/usr/bin/env python3

from math import prod

# input
V = [[int(i) for i in v[:-1]] for v in open("input.txt")]
N = len(V)
M = len(V[1])

# part 1
# top, right, bot, left
vis = [[[False, False, False, False] for _ in range(M)] for _ in range(N)]

for i in range(N):
    ML = -1
    MR = -1

    for j in range(M):
        if V[i][j] > ML:
            ML = V[i][j]
            vis[i][j][3] = True
        if V[i][-j-1] > MR:
            MR = V[i][-j-1]
            vis[i][-j-1][1] = True

for j in range(M):
    MT = -1
    MB = -1

    for i in range(N):
        if V[i][j] > MT:
            MT = V[i][j]
            vis[i][j][0] = True
        if V[-i-1][j] > MB:
            MB = V[-i-1][j]
            vis[-i-1][j][2] = True

print(sum(any(vis[i][j]) for i in range(N) for j in range(M)))

# part 2
# top, right, bot, left
nb = [[[0, 0, 0, 0] for _ in range(M)] for _ in range(N)]

for h in range(10):
    for i in range(N):
        LL = 0
        for j in range(M):
            if V[i][j] == h:
                nb[i][j][3] = j - LL
            if V[i][j] >= h:
                LL = j
        
        LR = M - 1
        for j in range(M - 1, -1, -1):
            if V[i][j] == h:
                nb[i][j][1] = LR - j
            if V[i][j] >= h:
                LR = j

    for j in range(M):
        LT = 0
        for i in range(N):
            if V[i][j] == h:
                nb[i][j][0] = i - LT
            if V[i][j] >= h:
                LT = i
        
        LB = N - 1
        for i in range(N - 1, -1, -1):
            if V[i][j] == h:
                nb[i][j][2] = LB - i
            if V[i][j] >= h:
                LB = i


print(max(prod(nb[i][j]) for i in range(N) for j in range(M)))
