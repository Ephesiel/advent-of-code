#!/usr/bin/env python3

from math import gcd, inf
from heapq import heappop, heappush

# input
V = [v[1:-2] for v in open("input.txt")][1:-1]

height = len(V)
width = len(V[0])
nb_rounds = height * width // gcd(height, width)

def values(default):
    return [[[default for _ in range(nb_rounds)] for _ in range(height)] for _ in range(width)]

path = values(True)

for x in range(width):
    for y in range(height):
        c = V[y][x]
        path[x][y][0] = '.' == c
        for t in range(1, nb_rounds):
            if '>' == c:
                path[(x + t) % width][y][t] = False
            if '<' == c:
                path[(x - t) % width][y][t] = False
            if 'v' == c:
                path[x][(y + t) % height][t] = False
            if '^' == c:
                path[x][(y - t) % height][t] = False

def neighbours(x, y, t):
    t1 = (t + 1) % nb_rounds
    
    # cas où on est sur la case de base
    if 0 == x and -1 == y:
        n = [(0, - 1, t1)]
        if path[0][0][t1]:
            n.append((0, 0, t1))
        return n

    # cas où on est sur la case de fin
    if width - 1 == x and height == y:
        n = [(width - 1, height, t1)]
        if path[width - 1][height - 1][t1]:
            n.append((width - 1, height - 1, t1))
        return n

    n = []

    if path[x][y][t1]:
        n.append((x, y, t1))

    if x < width - 1 and path[x + 1][y][t1]:
        n.append((x + 1, y, t1))

    if x > 0 and path[x - 1][y][t1]:
        n.append((x - 1, y, t1))

    if y < height - 1 and path[x][y + 1][t1]:
        n.append((x, y + 1, t1))

    if y > 0 and path[x][y - 1][t1]:
        n.append((x, y - 1, t1))
    
    return n

# dfs
def dfs(src, dstx, dsty):
    dist = values(inf)
    queue = [(0, src)]
    while queue:
        d, (x, y, t) = heappop(queue)

        if dstx == x and dsty == y:
            return (d + 1, (t + 1) % nb_rounds)
        
        for nx, ny, nt in neighbours(x, y, t):
            if -1 == ny:
                heappush(queue, (d + 1, (nx, ny, nt)))
            elif height == ny:
                heappush(queue, (d + 1, (nx, ny, nt)))
            elif d + 1 < dist[nx][ny][nt]:
                dist[nx][ny][nt] = d + 1
                heappush(queue, (d + 1, (nx, ny, nt)))

# part 1
d1, t1 = dfs((0, -1, 0), width - 1, height - 1)

print(d1)

# part 2
d2, t2 = dfs((width - 1, height, t1), 0, 0)
d3, t3 = dfs((0, -1, t2), width - 1, height - 1)

print(d1 + d2 + d3)

# part 3
# faire 1 000 000 allers retours

from_start = []
from_arrival = []

for t in range(nb_rounds):
    print(f"calcul {t}")
    from_start.append(dfs((0, -1, t), width - 1, height - 1))
    from_arrival.append(dfs((width - 1, height, t), 0, 0))

# nombre d'aller + retour
x = 2000000
s = 0
t = 0
for i in range(x):
    if 0 == i % 2:
        d, t = from_start[t]
    else:
        d, t = from_arrival[t]
    
    s += d

# on peut faire une recherche de pattern pour faire des nombres encore
# plus gros puisqu'il n'y a que nb_rounds * 2 possibilités
print(s)