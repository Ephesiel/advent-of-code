#!/usr/bin/env python3

# input
V = [eval(v[:-1]) for v in open("input.txt") if v != "\n"]

def compare(left, right):
    if isinstance(left, int) and isinstance(right, int):
        return 1 if left < right else 0 if left > right else -1
    if isinstance(left, int):
        left = [left]
    if isinstance(right, int):
        right = [right]
    
    n = len(left)
    m = len(right)

    for i in range(min(n, m)):
        v = compare(left[i], right[i])
        if -1 != v:
            return v
    
    return 1 if n < m else 0 if n > m else -1

# part 1
print(sum(compare(V[2*i], V[2*i + 1]) * (i + 1) for i in range(len(V)//2)))

# part 2
print((sum(compare(v, [[2]]) for v in V) + 1) * (sum(compare(v, [[6]]) for v in V) + 2))