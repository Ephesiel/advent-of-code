#!/usr/bin/env python3

# input
V = [v[:-1].split(': ') for v in open("input.txt")]
monkeys = {v[0]:v[1] for v in V}

def is_human(c):
    if 1 == len(c):
        return 'humn' == c[0]
    
    return is_human(c[1]) or is_human(c[2])

# invariant c1 contient 'humn'
def separate(c1, c2):
    # humn == c2 <=> humn == c2
    if 1 == len(c1):
        return c2

    op, a, b = c1

    # cas ou humn est dans a
    if is_human(a):
        # a + b == c2 <=> a == c2 - b
        if '+' == op:
            return separate(a, ['-', c2, b])
        # a - b == c2 <=> a == c2 + b
        if '-' == op:
            return separate(a, ['+', c2, b])
        # a * b == c2 <=> a == c2 / b
        if '*' == op:
            return separate(a, ['/', c2, b])
        # a / b == c2 <=> a == c2 * b
        if '/' == op:
            return separate(a, ['*', c2, b])
    
    # cas ou humn est dans b
    else:
        # a + b == c2 <=> b == c2 - a
        if '+' == op:
            return separate(b, ['-', c2, a])
        # a - b == c2 <=> b == a - c2
        if '-' == op:
            return separate(b, ['-', a, c2])
        # a * b == c2 <=> b == c2 / a
        if '*' == op:
            return separate(b, ['/', c2, a])
        # a / b == c2 <=> b == a / c2
        if '/' == op:
            return separate(b, ['/', a, c2])

def calcul(monkey, human):
    if human and 'humn' == monkey:
        return ['humn']

    if monkeys[monkey].isnumeric():
        return [monkeys[monkey]]
    
    a, op, b = monkeys[monkey].split()

    if human and 'root' == monkey:
        c1 = calcul(a, human)
        c2 = calcul(b, human)

        if is_human(c1):
            return separate(c1, c2)
        else:
            return separate(c2, c1)

    return [op, calcul(a, human), calcul(b, human)]

def to_string(c):
    if 1 == len(c):
        return c[0]
    
    op, a, b = c
    return '(' + to_string(a) + op + to_string(b) + ')'

print(eval(to_string(calcul('root', False))))

print(eval(to_string(calcul('root', True))))
