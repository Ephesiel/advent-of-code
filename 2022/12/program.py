#!/usr/bin/env python3

from heapq import heappop, heappush
from math import inf

# input
V = [[ord(c) for c in v[:-1]] for v in open("input.txt")]
m = len(V)
n = len(V[0])

def close(x, y, i, j):
    return\
    0 <= i < n and\
    0 <= j < m and\
    (\
        (ord('E') != V[j][i] and V[j][i] - 1 <= V[y][x]) or\
        (ord('E') == V[j][i] and ord('y') <= V[y][x])\
    )

def neighbours(x, y):
    return [(i, y) for i in [x-1,x+1] if close(x, y, i, y)] + [(x, j) for j in [y-1,y+1] if close(x, y, x, j)]

dist = [[inf]*n for _ in range(m)]
heap = []
for x in range(n):
    for y in range(m):
        if ord('S') == V[y][x]:
            V[y][x] = ord('a')
        # remove this line for part 1
        if ord('a') == V[y][x]:
            heap.append((0, (x, y)))
            dist[y][x] = 0

while heap:
    x, y = heappop(heap)[1]
    for i, j in neighbours(x, y):
        val = dist[y][x] + 1
        if val < dist[j][i]:
            dist[j][i] = val
            heappush(heap, (dist[j][i], (i, j)))
            if ord('E') == V[j][i]:
                print(dist[j][i])
                heap = []
                break