#!/usr/bin/env python3

from math import prod
from copy import deepcopy

# input
V = [v[:-1] for v in open("input.txt")]

S = []
M = []
m = 1

for i in range(1, len(V), 7):
    S.append(list(map(int, V[i].split(': ')[1].split(", "))))
    M.append((
        V[i + 1].split(': ')[1],
        int(V[i + 2].split()[-1]),
        int(V[i + 3].split()[-1]),
        int(V[i + 4].split()[-1])
    ))
    m *= M[-1][1]

def business(M, S, n, op):
    I = deepcopy(S)
    P = [0] * len(M)

    for _ in range(n):
        for i in range(len(M)):
            m = M[i]
            for old in I[i]:
                d = {'old': old}
                exec(m[0], globals(), d)
                new = op(d['new'])
                I[m[2] if 0 == new % m[1] else m[3]].append(new)
            P[i] += len(I[i])
            I[i] = []
    return P

# part 1
print(prod(sorted(business(M, S, 20, lambda x : x // 3))[-2:]))

# part 2
print(prod(sorted(business(M, S, 10000, lambda x : x % m))[-2:]))