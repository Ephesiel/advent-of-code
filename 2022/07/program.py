#!/usr/bin/env python3

# input
V = [v[:-1] for v in open("input.txt")][1:]

actual = [0, None, 0]
directories = [actual]
i = 1

for v in V:
    if '$ cd ..' == v:
        directories[actual[1]][2] += actual[2]
        actual = directories[actual[1]]
    elif '$ cd ' == v[:5]:
        actual = [i, actual[0], 0]
        directories.append(actual)
        i += 1
    elif '$ ls' == v:
        pass
    elif 'dir ' == v[:4]:
        pass
    else:
        actual[2] += int(v.split()[0])

while (None != actual[1]):
    directories[actual[1]][2] += actual[2]
    actual = directories[actual[1]]

# part 1
print(sum(i[2] for i in directories if i[2] <= 100000))

# part 2
print(min(i[2] for i in directories if i[2] >= -40000000 + directories[0][2]))