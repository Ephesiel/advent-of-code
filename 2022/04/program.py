#!/usr/bin/env python3

# input
V = [v[:-1] for v in open("input.txt")]

# part 1
print(sum(1 for x in ([list(map(int, v.replace(',', '-').split('-'))) for v in V]) if x[0] <= x[2] <= x[3] <= x[1] or x[2] <= x[0] <= x[1] <= x[3]))

# part 2
print(sum(1 for x in ([list(map(int, v.replace(',', '-').split('-'))) for v in V]) if x[0] <= x[2] <= x[1] or x[2] <= x[0] <= x[3]))