#!/usr/bin/env python3

# input
values = [v.split() for v in open("input.txt")]

# part 1
s = 0

for (x, y) in values:
    x = ord(x) - ord('A')
    y = ord(y) - ord('X')
    v = 3 if x == y else 0 if (x - 1) % 3 == (y % 3) else 6
    s += y + v + 1

print(s)

# part 2
s = 0
for (x, y) in values:
    x = ord(x) - ord('A')
    v = (ord(y) - ord('X')) * 3 
    y = x if 3 == v else (x - 1) % 3 if 0 == v else (x + 1) % 3
    s += y + v + 1

print(s)