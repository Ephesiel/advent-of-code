#!/usr/bin/env python3

from math import inf
from itertools import product

# input
V = [v.split(' ') for v in open("input.txt")]

V = [(v[1], int(v[4].split('=')[1][:-1]), [x[:-1] for x in v[9:]]) for v in V]

# trier selon le flot pour faire un codage des configurations
V = sorted(V, key=lambda x: x[1], reverse=True)

# permet de récupérer l'entier relatif au nom d'un noeud
N = {V[i][0]:i for i in range(len(V))}

# valeurs finales : uniquement des entiers
# (valeur flot, (indexs des voisins))
V = [(v[1], tuple(N[x] for x in v[2])) for v in V]

# part 1 : memoisation
last_flow = max(i for i in range(len(V)) if 0 != V[i][0]) + 1
configuration_max = 2**(last_flow)

# tableau de memoisation
memo = [[[-1] * configuration_max for _ in range(31)] for _ in range(len(V))]

# memoisation
def algo(node, minutes, configuration):
    # plus de temps
    if 0 >= minutes:
        return 0
    
    # valeur pas encore calculée
    if -1 == memo[node][minutes][configuration]:
    
        # le meilleur si on n'ouvre pas la valve
        m1 = max(algo(v, minutes - 1, configuration) for v in V[node][1])

        memo[node][minutes][configuration] = m1

        # mais on garde le meilleur en ouvrant la valve
        # (garde -> si la valve n'est pas encore ouverte dans la configuration actuelle)
        if 0 != V[node][0] and 0 == configuration & (1 << node):
            # valeur du flot final
            val = (minutes - 1) * V[node][0]
            
            # valeur maximale avec la nouvelle configuration (valve actuelle ouverte)
            m2 = max(algo(v, minutes - 2, configuration ^ (1 << node)) for v in V[node][1])

            # on garde le meilleur
            memo[node][minutes][configuration] = max(m1, m2 + val)
    
    return memo[node][minutes][configuration]
        
print(algo(N['AA'], 30, 0))

# part 2 : parcours de paires

# Floyd Warshall pour avior toutes les distances entre les valves
n = len(V)

w = [[inf] * n for _ in range(n)]
dist = [[inf] * n for _ in range(n)]

for i in range(n):
    for x in V[i][1]:
        w[i][x] = 1

for u, v in product(range(n), repeat=2):
    if u == v:
        dist[u][v] = 0
    if w[u][v] != inf:
        dist[u][v] = w[u][v]

for i, u, v in product(range(n), repeat=3):
    alt = dist[u][i] + dist[i][v]
    if dist[u][v] > alt:
        dist[u][v] = alt

def algo2(node1, node2, minutes1, minutes2, used):
    # tout le monde a fini
    if minutes1 <= 0 and minutes2 <= 0:
        return 0
    
    # pour ne pas avoir a répéter le code
    if minutes1 <= 0:
        minutes1, minutes2 = minutes2, minutes1
        node1, node2 = node2, node1
    
    m = 0
    
    # on n'a plus qu'un seul gars qui peut bouger
    if minutes2 <= 0:
        for u in range(last_flow):
            if not (used & (1 << u)):
                # temps d'aller jusqu'a la valve et de l'ouvrir
                t = dist[node1][u] + 1

                # valeur en partant de ce point
                val = algo2(u, None, minutes1 - t, -1, used ^ (1 << u))
                val += (minutes1 - t) * V[u][0]

                m = max(m, val)
        
        return m
    
    # les deux peuvent encore bouger

    # on fait la meme chose sur les paires
    for u in range(last_flow):
        if not (used & (1 << u)):
            for v in range(last_flow):
                if u != v and not (used & (1 << v)):
                    t1 = dist[node1][u] + 1
                    t2 = dist[node2][v] + 1

                    val = algo2(u, v, minutes1 - t1, minutes2 - t2, used ^ (1 << u) ^ (1 << v))
                    val += (minutes1 - t1) * V[u][0]
                    val += (minutes2 - t2) * V[v][0]

                    m = max(m, val)

    return m

for i in range(last_flow):
    print(i, V[i][0])

for i in range(last_flow):
    print(dist[i][:last_flow])

print(algo2(N['AA'], N['AA'], 30, -1, 0))