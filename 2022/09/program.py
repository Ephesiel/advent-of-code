#!/usr/bin/env python3

# input
V = [v[:-1] for v in open("input.txt")]

pts = [set() for _ in range(10)]
knots = [[0,0] for _ in range(10)]

def move(H,Q):
    dx = abs(Q[0] - H[0])
    dy = abs(Q[1] - H[1])

    if 1 < dx or (1 == dx and 2 == dy):
        Q[0] += 1 if H[0] > Q[0] else -1
    if 1 < dy or (1 == dy and 2 == dx):
        Q[1] += 1 if H[1] > Q[1] else -1


for v in V:
    d, n = v.split()
    for i in range(int(n)):
        knots[0][0] += 1 * (-1 if 'L' == d else 1 if 'R' == d else 0)
        knots[0][1] += 1 * (-1 if 'D' == d else 1 if 'U' == d else 0)

        for i in range(9):
            move(knots[i], knots[i+1])
            pts[i+1].add((knots[i+1][0], knots[i+1][1]))

# part 1
print(len(pts[1]))

# part 2
print(len(pts[9]))