#!/usr/bin/env pypy

from itertools import product
from time import time

# input
V = [[x.split() for x in v.split('.')] for v in open("input.txt")]
blueprints = [{
    "ore":      {"ore": int(v[0][-2])},
    "clay":     {"ore": int(v[1][-2])},
    "obsidian": {"ore": int(v[2][-5]), "clay": int(v[2][-2])},
    "geode":    {"ore": int(v[3][-5]), "obsidian": int(v[3][-2])}
} for v in V]

def algo(blueprint, memo, minutes, minerals = (0, 0, 0), robots = (1, 0, 0, 0)):
    if 0 == minutes:
        return 0

    if (minerals, robots) in memo[minutes - 1]:
        return memo[minutes - 1][(minerals, robots)]

    n_ore = minerals[0]
    n_clay = minerals[1]
    n_obsidian = minerals[2]

    r_ore = robots[0]
    r_clay = robots[1]
    r_obsidian = robots[2]
    r_geode = robots[3]

    # si on ne crée pas de nouveau robot
    next_minerals = (
        n_ore + r_ore,
        n_clay + r_clay,
        n_obsidian + r_obsidian
    )

    n_geode = algo(blueprint, memo, minutes - 1, next_minerals, robots)

    # Si on crée un nouveau robot ore
    if n_ore >= blueprint["ore"]["ore"]:
        next_minerals = (
            n_ore + r_ore - blueprint["ore"]["ore"],
            n_clay + r_clay,
            n_obsidian + r_obsidian
        )
        next_robots = (
            r_ore + 1,
            r_clay,
            r_obsidian,
            r_geode
        )

        n_geode = max(n_geode, algo(blueprint, memo, minutes - 1, next_minerals, next_robots))
    
    # Si on crée un nouveau robot clay
    if n_ore >= blueprint["clay"]["ore"]:
        next_minerals = (
            n_ore + r_ore - blueprint["clay"]["ore"],
            n_clay + r_clay,
            n_obsidian + r_obsidian
        )
        next_robots = (
            r_ore,
            r_clay + 1,
            r_obsidian,
            r_geode
        )

        n_geode = max(n_geode, algo(blueprint, memo, minutes - 1, next_minerals, next_robots))
    
    # Si on crée un nouveau robot obsidian
    if n_ore >= blueprint["obsidian"]["ore"] and n_clay >= blueprint["obsidian"]["clay"]:
        next_minerals = (
            n_ore + r_ore - blueprint["obsidian"]["ore"],
            n_clay + r_clay - blueprint["obsidian"]["clay"],
            n_obsidian + r_obsidian
        )
        next_robots = (
            r_ore,
            r_clay,
            r_obsidian + 1,
            r_geode
        )

        n_geode = max(n_geode, algo(blueprint, memo, minutes - 1, next_minerals, next_robots))
    
    # Si on crée un nouveau robot geode
    if n_ore >= blueprint["geode"]["ore"] and n_obsidian >= blueprint["geode"]["obsidian"]:
        next_minerals = (
            n_ore + r_ore - blueprint["geode"]["ore"],
            n_clay + r_clay,
            n_obsidian + r_obsidian - blueprint["geode"]["obsidian"]
        )
        next_robots = (
            r_ore,
            r_clay,
            r_obsidian,
            r_geode + 1
        )

        n_geode = max(n_geode, algo(blueprint, memo, minutes - 1, next_minerals, next_robots))

    memo[minutes - 1][(minerals, robots)] = n_geode + r_geode
    return n_geode + r_geode
    
def maximize_geode(blueprint, minutes):
    memo = [{} for _ in range(minutes)]
    return algo(blueprint, memo, minutes)

# part 1
# c = time()
# somme = 0
# for i in range(len(blueprints)):
#     val = maximize_geode(blueprints[i], 24)
#     somme += val * (i + 1)
#     print(val, val * (i + 1), somme, time() - c)
#     c = time()

c = time()
somme = 1
for i in range(3):
    val = maximize_geode(blueprints[i], 32)
    somme *= val
    print(val, somme, time() - c)
    c = time()