#!/usr/bin/env python3

from copy import deepcopy

# input
V = [v[:-1] for v in open("input.txt")]

nb_stacks = (len(V[0]) + 1) // 4
separator = [i for i in range(len(V)) if V[i] == ''][0]

stacks = [[] for i in range(nb_stacks)]

for v in V[:separator-1]:
    for i in range(len(v) // 4 + 1):
        if v[4*i+1] != ' ':
            stacks[i].append(v[4*i+1])

for s in stacks:
    s.reverse()

# part 1
stacks1 = deepcopy(stacks)

for v in V[separator+1:]:
    (_, move, _, fr, _, to) = v.split()
    for _ in range(int(move)):
        stacks1[int(to) - 1].append(stacks1[int(fr) - 1].pop())

for s in stacks1:
    print(s[-1], end="")
print()

# part 2
for v in V[separator+1:]:
    (_, move, _, fr, _, to) = v.split()

    n = stacks[int(fr) - 1][-int(move):]
    del stacks[int(fr) - 1][-int(move):]

    stacks[int(to) - 1] += n

for s in stacks:
    print(s[-1], end="")
print()