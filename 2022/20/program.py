#!/usr/bin/env pypy

class Node:
    def __init__(self, val):
        self.val = val
        self.next = None
        self.prev = None

def remove_node(node):
    node.next.prev = node.prev
    node.prev.next = node.next

# part 1
mult = 1
nb_turn = 1

# part 2
mult = 811589153
nb_turn = 10

# input
V = [Node(int(v) * mult) for v in open("input.txt")]
n = len(V)
index_0 = 0

for i in range(n):
    V[i].prev = V[i - 1]
    V[i].next = V[(i + 1) % n]

    if 0 == V[i].val:
        index_0 = i

for i in range(nb_turn):
    for i in range(n):
        node = V[i]
        x = V[i].val

        if 0 != x:
            remove_node(node)

        if x > 0:
            for _ in range(x % (n-1)):
                node = node.next
            
            V[i].next = node.next
            V[i].prev = node
            node.next.prev = V[i]
            node.next = V[i]

        if x < 0:
            for _ in range((-x) % (n-1)):
                node = node.prev
            
            V[i].next = node
            V[i].prev = node.prev
            node.prev.next = V[i]
            node.prev = V[i]
    
node = V[index_0]
vals = []

for _ in range(3):
    for _ in range(1000):
        node = node.next
    vals.append(node.val)

print(sum(vals))