#!/usr/bin/env python3

from itertools import product

# input
V = [tuple(map(int, v.split(','))) for v in open("input.txt")]

M = max(v[i] for v in V for i in [0, 1, 2]) + 1

C = [[[False for _ in range(M+1)] for _ in range(M+1)] for _ in range(M+1)]

def nb_size_empty(x, y, z):
    v = 6
    v -= C[x-1][y][z]
    v -= C[x+1][y][z]
    v -= C[x][y-1][z]
    v -= C[x][y+1][z]
    v -= C[x][y][z-1]
    v -= C[x][y][z+1]
    return v

s = 0

for x, y, z in V:
    C[x][y][z] = True

for x, y, z in product(range(M), repeat=3):
    if C[x][y][z]:
        s += nb_size_empty(x,y,z)

# part 1
print(s)

# part 2
def dfs(x, y, z, visited, neighbours):
    inside = True
    cc = []
    stack = [(x, y, z)]

    while stack:
        i,j,k = stack.pop()
        if not visited[i][j][k]:
            visited[i][j][k] = True
            cc.append((i, j, k))
        
            if 0 == x or 0 == y or 0 == z:
                inside = False

            for i0, j0, k0 in neighbours[i][j][k]:
                stack.append((i0,j0,k0))
    
    return (cc, inside)

def connected_components():
    visited = [[[False for _ in range(M+1)] for _ in range(M+1)] for _ in range(M+1)]
    neighbours = [[[[] for _ in range(M+1)] for _ in range(M+1)] for _ in range(M+1)]

    for x, y, z in product(range(M), repeat=3):
        if not C[x][y][z]:
            if not C[x-1][y][z]: neighbours[x][y][z].append((x-1,y,z))
            if not C[x+1][y][z]: neighbours[x][y][z].append((x+1,y,z))
            if not C[x][y-1][z]: neighbours[x][y][z].append((x,y-1,z))
            if not C[x][y+1][z]: neighbours[x][y][z].append((x,y+1,z))
            if not C[x][y][z-1]: neighbours[x][y][z].append((x,y,z-1))
            if not C[x][y][z+1]: neighbours[x][y][z].append((x,y,z+1))
        else:
            visited[x][y][z] = True

    ccs = []
    
    for x, y, z in product(range(M), repeat=3):
        if not visited[x][y][z]:
            cc, inside = dfs(x, y, z, visited, neighbours)
            if inside:
                ccs.append(cc)
    
    return ccs

for cc in connected_components():
    for x, y, z in cc:
        s -= 6 - nb_size_empty(x, y, z)

print(s)