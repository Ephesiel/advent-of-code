#!/usr/bin/env pypy

class Grid:
    def __init__(self, grid):
        n = len(grid)
        m = len(grid[0])

        self.grid_height = n
        self.grid_width = m

        self.grid = [['#' == c for c in l] for l in grid]
        self.nb_elves = sum(sum(g) for g in self.grid)
        self.nb_rounds = 0

        self.verify_borders()
    
    def display_grid(self):
        print()
        for g in self.grid:
            print(''.join(['#' if c else '.' for c in g]))
    
    def verify_borders(self):
        self.northern_elf = self.grid_height
        self.southern_elf = 0
        self.western_elf = self.grid_width
        self.eastern_elf = 0

        for y in range(self.grid_height):
            for x in range(self.grid_width):
                if self.grid[y][x]:
                    self.northern_elf = min(self.northern_elf, y)
                    self.southern_elf = max(self.southern_elf, y)
                    self.western_elf = min(self.western_elf, x)
                    self.eastern_elf = max(self.eastern_elf, x)

        if self.grid_height - 1 == self.southern_elf:
            self.grid_height += 1
            self.grid += [[False] * self.grid_width]
        
        if 0 == self.northern_elf:
            self.grid_height += 1
            self.grid = [[False] * self.grid_width] + self.grid
            self.southern_elf += 1
            self.northern_elf += 1

        if self.grid_width - 1 == self.eastern_elf:
            self.grid_width += 1
            for i in range(self.grid_height):
                self.grid[i].append(False)
        
        if 0 == self.western_elf:
            self.grid_width += 1
            for i in range(self.grid_height):
                self.grid[i] = [False] + self.grid[i]
            self.eastern_elf += 1
            self.western_elf += 1
    
    def can_move(self, x, y):
        return any(self.grid[y + i][x + j] for i in [1, 0, -1] for j in [1, 0, -1] if i != 0 or j != 0)

    def try_move(self, x, y, moves):
        if 0 == len(moves):
            return None
        
        move = moves[0]

        if 'north' == move:
            if not any(self.grid[y - 1][x + i] for i in [-1, 0, 1]):
                return (x, y - 1)

        if 'south' == move:
            if not any(self.grid[y + 1][x + i] for i in [-1, 0, 1]):
                return (x, y + 1)

        if 'west' == move:
            if not any(self.grid[y + i][x - 1] for i in [-1, 0, 1]):
                return (x - 1, y)

        if 'east' == move:
            if not any(self.grid[y + i][x + 1] for i in [-1, 0, 1]):
                return (x + 1, y)
        
        return self.try_move(x, y, moves[1:])
    
    def round(self):
        moves = [[[] for _ in range(self.grid_width)] for _ in range(self.grid_height)]

        moves_order = ['north', 'south', 'west', 'east']
        for i in range(self.nb_rounds % 4):
            moves_order = moves_order[1:] + [moves_order[0]]

        for y in range(self.northern_elf, self.southern_elf + 1):
            for x in range(self.western_elf, self.eastern_elf + 1):
                if self.grid[y][x] and self.can_move(x, y):
                    coords = self.try_move(x, y, moves_order)
                    if None != coords:
                        new_x, new_y = coords
                        moves[new_y][new_x].append((x, y))
        
        moved = False

        for y in range(self.grid_height):
            for x in range(self.grid_width):
                if 1 == len(moves[y][x]):
                    old_x, old_y = moves[y][x][0]
                    self.grid[old_y][old_x] = False
                    self.grid[y][x] = True
                    moved = True

        self.verify_borders()
        self.nb_rounds += 1

        return moved
    
    def empty_grounds(self):
        return (self.southern_elf - self.northern_elf + 1) * (self.eastern_elf - self.western_elf + 1) - self.nb_elves

    def part1(self, n):
        for _ in range(n):
            self.round()
        return self.empty_grounds()

    def part2(self):
        i = 1
        while self.round():
            i += 1
        return i

# input
V = [v[:-1] for v in open("input.txt")]

# part 1
g = Grid(V)
print(g.part1(10))

# part 2
g = Grid(V)
print(g.part2())