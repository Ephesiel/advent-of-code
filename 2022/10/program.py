#!/usr/bin/env python3

# input
V = [v[:-1] for v in open("input.txt")]
s = 0
X = 1
i = 0
p = ["" for _ in range(6)]

for c in range(1, 241):
    if 0 == (c - 20) % 40:
        s += c * X
    
    crt = (c - 1) % 40
    ind = (c - 1) // 40
    p[ind] += '#' if crt in [X-1, X, X+1] else '.'
    
    if isinstance(V[i], int):
        X += V[i]
    elif "addx" == V[i][:4]:
        V[i] = int(V[i][5:])
        continue
    i += 1

# part 1
print(s)

# part 2
for x in p:
    print(x)