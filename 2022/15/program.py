#!/usr/bin/env python3

# input
V = [v[:-1] for v in open("input.txt")]

def app(x, y, t):
    # on vérifie que [x, y] n'est pas déja inclu
    for i, j in t:
        if i <= x and y <= j:
            return t
    
    change = True

    while change:
        change = False
        for w in range(len(t)):
            i, j = t[w]
            # [i, j] est inclus dans [x, y] on peut le virer
            if x <= i and j <= y:
                del t[w]
                change = True
                break
            # la droite de [i, j] est inclus dans [x, y]
            if i <= x <= j + 1:
                del t[w]
                change = True
                x = i
                break
            # la gauche de [i, j] est inclus dans [x, y]
            if i - 1 <= y <= j:
                del t[w]
                change = True
                y = j
                break
    
    t.append((x, y))
    return t

part1_line = 2000000
number_of_lines = 4000001
not_possible = [[] for _ in range(number_of_lines)]
    
for v in V:
    s, b = v.split(':')
    sx, sy = s.split(',')
    bx, by = b.split(',')

    sensor = (int(sx.split('=')[1]), int(sy.split('=')[1]))
    beacon = (int(bx.split('=')[1]), int(by.split('=')[1]))

    distance = abs(sensor[0] - beacon[0]) + abs(sensor[1] - beacon[1])
    first_line = max(0,              sensor[1] - distance)
    last_line = min(number_of_lines, sensor[1] + distance + 1)

    # pour vérifier que c'est pas trop long pour chaque sensor
    print(sensor, beacon)
    
    for i in range(first_line, last_line):
        distance_to_line = abs(sensor[1] - i)
        distance_on_line = distance - distance_to_line

        not_possible[i] = app(
            sensor[0] - distance_on_line,
            sensor[0] + distance_on_line,
            not_possible[i]
        )

# result 1
print(sum(p[1] - p[0] for p in not_possible[part1_line]))

# result 2
for i in range(number_of_lines):
    for p in not_possible[i]:
        if p[1] < number_of_lines - 1:
            print((p[1] + 1) * 4000000 + i)